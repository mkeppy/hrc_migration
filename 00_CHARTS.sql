use [hrc_worknet]
go


---CHART INDUSTRY
	  

	select oi.[IndustryCode], ti.[IndustryDesc]  , Count(*) [Count]
	from [dbo].[tblOrganization] as org 
	inner join [dbo].[tblOrgIndustry] as oi on org.[OrgID]=oi.OrgID
		left join [dbo].[tlkOrgIndustry] as ti on oi.[IndustryCode]= ti.[IndustryAbbrev]
 	group by oi.[IndustryCode], ti.[IndustryDesc] 
	order by oi.[IndustryCode]


	
---CHART ORG_TYPE
	  

	select org.[OrgTypeID], ot.[TypeDesc] as OrgTypeDesc, Count(*) as [Count]
	from [dbo].[tblOrganization] as org 
 		left join [dbo].[tlkOrgType] as ot on [org].[OrgTypeID] = [ot].[TypeID]
 	group by  org.[OrgTypeID], ot.[TypeDesc]
	order by  org.[OrgTypeID], ot.[TypeDesc]


	 
-- CHART ATTRIBUTES



		select l.[AttributeID], l.[AttributeValueID], l.[AttributeValue], Count(*) RecordCount 
		into hrc_fnc_maps.dbo.chart_attribute_new
		from [hrc_fnc_worknet].[dbo].[tblAttribute] a
		left join [hrc_fnc_worknet].[dbo].[tlkLookupValue] l on [a].[AttributeID] = [l].[AttributeValueID]
		where l.[AttributeID]='13'
		group by l.[AttributeID], l.[AttributeValueID],  l.[AttributeValue]
		order by l.[AttributeValueID], l.[AttributeValue]

		select * 
		from hrc_fnc_maps.dbo.chart_attribute_new t1
		left join [hrc_fnc_maps].dbo.[CHART_Attribute] as t2 on t1.[AttributeValueID]=t2.[AttributeValueID]
		where t2.[AttributeValueID] is null 


			select l.[AttributeID], l.[AttributeValueID], l.[AttributeValue], a.[Description]
	 		from [hrc_fnc_worknet].[dbo].[tblAttribute] a
			left join [hrc_fnc_worknet].[dbo].[tlkLookupValue] l on [a].[AttributeID] = [l].[AttributeValueID]
			where l.[AttributeID]='13' and l.[AttributeValueID]='2384'
		 	order by l.[AttributeValueID], l.[AttributeValue]

--chart Person type
 

		select * 
		into hrc_fnc_maps.dbo.CHART_PersonType
		from [hrc_fnc_worknet].[dbo].[tlkLookupValue] where [AttributeID]='3' 
		order by [AttributeValueID]

		select * 
		from  hrc_fnc_maps.dbo.CHART_PersonType t
		left join [hrc_fnc_maps].dbo.chart_persontype t2 on t.[AttributeValueID]=t2.[AttributeValueID]
		where t2.[AttributeValueID] is null
 

  