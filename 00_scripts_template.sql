USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX
			WHERE   SF_Object LIKE '%XXXXXXXXXXXX%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_XXXXXXXX

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,fields here
							,fields here
							,fields here
			 
			 
			 		--	INTO [hrc_fnc_migration].DBO.IMP_XXXXXXXXXXXX
			
					FROM [hrc_fnc_worknet].dbo.TBL_NAME AS T1
				 
			 

				 
END -- TC1: XXXXXX

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_XXXXXXXXXXXX
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_XXXXXXXXXXXX GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 