USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.[tblOrgERG]
			WHERE   SF_Object LIKE '%emp%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 
 
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 							,t1.[OrgERGID] as Legacy_Id__c
					 		,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,T1.GroupName  AS  ERG_Name__c		
							,T1.WebURL  AS  Website__c		
							,T1.Phone  AS  Phone__c		
							,T1.Email  AS  Email__c		
 			 
							,t1.[OrgID] as zrefOrgId
							,T1.GroupName as zrefGroupName
			 		INTO [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP
 					FROM [hrc_fnc_worknet].dbo.[tblOrgERG] AS T1
				 	inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
				 
		 

				 
END -- TC1: 1719  fnc 1689

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT * FROM [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP
	where [ERG_Name__c] is null 
	
	 update [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP
	 set [ERG_Name__c] ='Unknown'
	 where [ERG_Name__c] is null 
	 --33 

	select COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP
	
	select * from [hrc_fnc_migration].DBO.IMP_EMPLOYEE_RESOURCE_GROUP order by zrefOrgId

END 