use [hrc_fnc_migration]
go


begin --IMP_CONTACT_UPDATE... 

				select  c.id, t2.[Person_Type__c], 
						p.[PersonID] zrefPersonId, p.[Email] zpsEmail, p.[FirstName] as zrefFname, p.[LastName] as zrefLname,
						c.name as zsfName, c.email as zsfEmail, c.[LEGACY_ID__C] zrefLegId--, x.[USERNAME] zrefUserN
				
				 into [hrc_fnc_migration].dbo.IMP_CONTACT_UPDATE_knowwho_person_type
				from  [hrc_fnc_worknet].dbo.[tblPerson] p
				inner join [hrc_fnc_migration].dbo.tbl_person_dnc t on [p].[PersonID] = [t].[PersonID]
				inner join [hrc_fnc_migration].dbo.xtr_contact c on p.[Email]=c.email
			 	inner join [hrc_fnc_migration].dbo.[XTR_USERS] x on c.createdbyid=x.id
				inner join (select * from [hrc_fnc_migration].dbo.tbl_person_type where [Person_Type__c] like '%LEG - LGBT Staffer%') as t2 on t.[PersonID]=t2.[PersonID]
				where p.[Email] is not null and p.[Email]!='' and c.[LEGACY_ID__C]=''
				order by p.[LastName], p.[FirstName]

end

begin --audit
			--dupe
				select * from [hrc_fnc_migration].dbo.IMP_CONTACT_UPDATE_knowwho_person_type
				where id in (select id from [hrc_fnc_migration].dbo.IMP_CONTACT_UPDATE_knowwho_person_type group by id having Count(*)>1)

			--remove dupe
				delete [hrc_fnc_migration].dbo.IMP_CONTACT_UPDATE_knowwho_person_type
				where id in (select id from [hrc_fnc_migration].dbo.IMP_CONTACT_UPDATE_knowwho_person_type group by id having Count(*)>1)
				and (zrefPersonId='57836' or [zrefPersonId]='60039' or [zrefPersonId]='57417')
				
end		 