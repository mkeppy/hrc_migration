use [hrc_fnc_worknet]
go

 
BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.[tlkStaff]
			WHERE   SF_Object LIKE '%user%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 

 select * from [hrc_fnc_migration].dbo.xtr_profile
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_USER

END 

BEGIN -- CREATE IMP 

						select   [hrc_fnc_migration].[dbo].[fnc_OwnerId]()   AS OwnerId   
 							,T1.FirstName  AS  FirstName	-- ONLY create User records for Staff linked from other tables (tblInteraction)
							,T1.LastName  AS  LastName	
							,T1.Email    AS  Email	
							,T1.Email   AS  Username	-- Migrate as tlkStaff.Email + ".test"
							,'00e5A000001lrOlQAI'  AS  ProfileID	-- Migrate as "HRC Test User"
							,'FALSE'  AS  IsActive	-- Migrate all as FALSE
							,'ISO-8859-1' AS  EmailEncodingKey	-- Migrate all as "General US & Western Europe"
							,LEFT(T1.FirstName,1) + LEFT(T1.LastName,4) as  Alias	-- Migrate as LEFT(FirstName,1) + LEFT(LastName,4)
							,LEFT(T1.FirstName,1) + LEFT(T1.LastName,4) as  CommunityNickname
							,'America/New_York'  AS  TimeZoneSidKey	-- Migrate all as Eastern Daylight Time
							,'en_US' as LanguageLocaleKey
 							,'en_US' as LocaleSidKey
 							,T1.[StaffID] as zrefStaffId
							,x.id zrefUserID
							,x.username as zrefUserNAme
			  	 	    into [hrc_fnc_migration].DBO.IMP_USER
			
						from [hrc_fnc_worknet].dbo.[tlkStaff] AS T1
						inner join [hrc_fnc_migration].dbo.tbl_users as t on t1.[StaffID]=t.StaffId
						left join [hrc_fnc_migration].dbo.xtr_users x on t1.[Email]=x.email
						order by t1.staffid
					
END -- TC1: 42   fnc 42
 
BEGIN -- AUDIT
			select   *
			from     [hrc_fnc_migration].[dbo].[IMP_USER]
			where    [Username] in (
									   select   [Username]
									   from     [hrc_fnc_migration].[dbo].[IMP_USER]
									   group by [Username]
									   having   Count(*) > 1
								   )
			order by [Username];

		 	SELECT * FROM [hrc_fnc_migration].DBO.IMP_USER 
			where [Username] is null
			order by lastname, firstname
			
			--update only on the IMP table to use in subsequent upload of records when looking up for the system admin
			update [hrc_fnc_migration].DBO.IMP_USER 
			set [Username]='admin@hrc.org'
			where [FirstName]='System' and [LastName]='User'
			


END 