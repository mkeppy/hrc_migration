USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.[tblInteraction]
			WHERE   SF_Object LIKE '%task%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 

 
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_TASK

END 
 
BEGIN -- CREATE IMP 

					SELECT   case when x1.id is not null then x1.id else [hrc_fnc_migration].[dbo].[fnc_OwnerId]()  end as OwnerId   
 							,T1.InteractionID  AS  Legacy_ID__c		
				 			,x2.id  AS  WhatId		-- Yes/Link(tblOrganization.OrgID)
				 			,x3.id  AS  WhoId		-- Yes/Link(tblPerson.PersonID)
 							,case when t1.[InteractionDate] is not null then Cast(T1.InteractionDate  as Date) 
								  when t1.[InteractionDate] is null then Cast(t1.[DateCreated] as Date) end as  ActivityDate		
							,t2.[AttributeValue]  AS  Subtype__c	-- On tlkLookupValue, AttributeID=20 are the InteractionType values	-- Yes/link(tlkLookupValue.AttributeValueID)
							,t3.[AttributeValue]  AS  [Subject]	-- On tlkLookupValue, AttributeID=21 are the InteractionMethod values	-- Yes/link(tlkLookupValue.AttributeValueID)
							,case T1.InteractionStatus when 'C' then 'Completed/Closed'
													   when 'W' then 'HRC Waiting on Input'
												       when 'R' then 'HRC Researching'
													   when 'F' then 'HRC Follow-Up'
													   when 'P' then 'Recipient in Process'
													   when 'D' then 'Recipient Declined' end as  [Status]	-- Migrate as full words: C=Completed/Closed, W=HRC Waiting on Input, R=HRC Researching, F=HRC Follow-Up, P=Recipient in Process, D=Recipient Declined	
							,T1.[Description]  AS  [Description]		
							,Cast(t1.[DateCreated] as Date) as CreatedDate
				 
							,T1.InteractionStatus  as zrefIntStat
							,t1.[OrgID] as zrefOrgID
							,t1.StaffOwnerId  as zrefStafOwId
							,x2.[name] as zrefOrgName
							,t1.[PersonID] as zrefPersonId
							,x3.legacy_Id__c as zrefContactLegID
							,x3.[name] as zrefContactName

							,t5.[OrgOfficialName] zrefDNC_OrgName
							,t5.[OrgID] zrefDNC_OrgId
							,t6.[PersonID] zrefDNC_PersonId
						
			 		  into [hrc_fnc_migration].DBO.IMP_TASK
			
					FROM [hrc_fnc_worknet].dbo.[tblInteraction] AS T1
					left join  (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_fnc_worknet].dbo.[tlkLookupValue] where [AttributeID]='20' ) as t2 on t1.[InteractionTypeID]=t2.[AttributeValueID]

					left join  (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_fnc_worknet].dbo.[tlkLookupValue] where [AttributeID]='21' ) as t3 on t1.[InteractionMethodID]=t3.[AttributeValueID]
					left join [hrc_fnc_migration].dbo.IMP_USER as t4 on t1.StaffOwnerId = t4.zrefStaffId
					left join [hrc_fnc_migration].dbo.xtr_users as x1 on t4.UserName = x1.UserName
				 	left join [hrc_fnc_migration].dbo.xtr_account as x2 on Cast(t1.[OrgID] as Varchar(20))=x2.Legacy_Id__c
				    left join [hrc_fnc_migration].dbo.xtr_contact as x3 on t1.[PersonID]=x3.Legacy_Id__c
					
					--dnc 
					left join [hrc_fnc_migration].dbo.tblOrganization_dnc t5 on t1.[OrgID]=t5.[OrgID]
					left join [hrc_fnc_migration].dbo.tbl_person_dnc  as t6 on t1.[PersonID]=t6.[PersonID]
				 	where x2.id is not null or x3.id is not null 

	 
					 
END -- TC1: 25,106.  fnc 22852

BEGIN -- AUDIT

	SELECT * FROM [hrc_fnc_migration].DBO.IMP_TASK
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_TASK GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_TASK
	SELECT * FROM [hrc_fnc_migration].DBO.IMP_TASK
	SELECT distinct [Subtype__c] FROM [hrc_fnc_migration].DBO.IMP_TASK

	--udpate Status when null 
	update [hrc_fnc_migration].dbo.IMP_TASK
	set [Status] = 'Completed/Closed'
	where [Status] is null 

	--udpate Subject when null 
	update [hrc_fnc_migration].dbo.IMP_TASK
	set [Subject] =  zrefContactName
	where [Subject] is null or [Subject]=''
	
	select * from [hrc_fnc_migration].dbo.IMP_TASK where [Subject] is null

	update [hrc_fnc_migration].dbo.IMP_TASK
	set [Subject] =  zrefOrgName
	where [Subject] is null and whatid is not null 
	
	 select * from [hrc_fnc_migration].dbo.IMP_TASK 


END 

select * from [hrc_fnc_migration].dbo.xtr_account
where legacy_id__c in (select [Legacy_Id__c] from [hrc_fnc_migration].dbo.xtr_contact group by [Legacy_Id__c] having Count(*)>1)

select * from [hrc_fnc_migration].dbo.xtr_contact
where legacy_id__c =''

