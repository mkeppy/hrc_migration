USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tblOrgLocation
			WHERE   SF_Object LIKE '%org%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,T1.OrgLocationID  AS  Legacy_ID__c		
					 	 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,T1.City  AS  City__c		
							,T1.[State]  AS  State__c		
							,T1.Zip  AS  Zip__c		
							,T1.CurrentEmployeeNbr  AS  Current_Number_of_Employees__c		
							,T1.Country  AS  Country__c		
			 
							,t1.[OrgID] as zrefOrgId
			 
			 	  INTO [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION
			
					FROM [hrc_fnc_worknet].dbo.tblOrgLocation AS T1
					inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20)) =x1.legacy_id__c
				 
	 
				 
END -- TC1:  11,070   fnc: 11237

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION
	 
	 
	SELECT * FROM [hrc_fnc_migration].DBO.IMP_ORGANIZATION_LOCATION
	order by organization__c
	
END 