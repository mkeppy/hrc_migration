USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tblOrgProgram
			WHERE   SF_Object LIKE '%prog%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tblOrgProgram 
			WHERE  SF_Object_2 LIKE '%prog%'  OR  [Translation_Rules_2] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_PROGRAM
	DROP TABLE [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT

END 

BEGIN -- CREATE IMP_PROGRAM 

					select   distinct [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId 
							 ,T2.[AttributeValue]  AS  Name	-- Create ONE Program__c record per unique ProgramID value. Migrate the [AttributeValue] as the record Name, via the link.	-- Yes/Link(tlkLookupValue.AttributeValueID)
							 
							 ,T1.ProgramID as zrefProgramId

		 		 	into [hrc_fnc_migration].dbo.IMP_PROGRAM 
					from [hrc_fnc_worknet].dbo.[tblOrgProgram] AS T1
					left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_fnc_worknet].dbo.[tlkLookupValue] 
								where [AttributeID]='1') as t2 on t1.[ProgramID]=t2.[AttributeValueID]
 					order by t1.[ProgramID]

					select * from [hrc_fnc_migration].dbo.IMP_PROGRAM 

end

begin --CREATE IMP_PROGRAM_INVOLVEMENT
					
					select   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,T1.ProgramRecID  AS  Legacy_ID__c		
						 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
						 	,x2.id  AS  Program__c		-- Yes/Link(tlkLookupValue.AttributeValueID)
							,case T1.StatusAbbrev when 'N' then 'New' 
											      when 'P' then 'Pending Approval'
												  when 'A' then 'Approved'
												  when 'T' then 'Internal'
												  when 'I' then 'Inactive'
												  when 'D' then 'Defunct' 
												  end as  Status__c	-- Migrate as: N=New, P=Pending Approval, A=Approved, T=Internal, I=Inactive, D=Defunct	
							,case T1.OKToRateInd when 1 then 'TRUE' else 'FALSE' end  AS  Show_Rating_on_Web__c	-- If OKToRateInd=1, then TRUE. Else FALSE.	
							,case T1.SendSurveyInd when 1 then 'TRUE' else 'FALSE' end AS  Send_Suvey__c	-- If SendSurveyInd=1, then TRUE. Else FALSE.	
							,T1.Rating  AS  Internal_Rating__c		
							,T1.RatingWeb  AS  Web_Rating__c		
							,T1.WebNotes  AS  Web_Notes__c		
							,Cast(T1.DateLastUpdated  as Date) as  Legacy_Last_Modified_Date__c		
							,t3.[FirstName] + ' ' + t3.[LastName] AS Legacy_Last_Modified_By__c  	-- Migrate as FirstName + " " + LastName from tlkStaff table via link.	-- Yes/Link(tlkStaff.StaffID)
			 
							,t1.[OrgID] as zrefOrgId
							,t1.[ProgramID] as zrefProgramId
							 
					 INTO [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT
			
					from [hrc_fnc_worknet].dbo.[tblOrgProgram] AS T1
					left join [hrc_fnc_worknet].dbo.[tlkStaff] as t3 on [T1].[LastUpdatedByStaffID] = [t3].[StaffID]
					inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20)) =x1.legacy_id__c
 					left join [hrc_fnc_migration].dbo.imp_program as t2 on t1.[ProgramID]=t2.zrefProgramId
					left join [hrc_fnc_migration].dbo.xtr_program as x2 on t2.[Name]= x2.[Name]
				  
				 
END -- TC1: 24541   fnc: 21332

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	select Web_Notes__c , Len(Cast(Web_Notes__c as Varchar(max))) as le
	from [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT
	order by le desc 


  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT
	 
	 select * from [hrc_fnc_migration].DBO.IMP_PROGRAM_INVOLVEMENT 
	 order by [zrefOrgId]
END 