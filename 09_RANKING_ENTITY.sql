USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tlkRankEntity
			WHERE   SF_Object LIKE '%ra%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_RANKING_ENTITY

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,T1.RankEntityID  AS  Rank_Entity_ID__c		
							,T1.RankEntityName  AS  [Name]		
							,T1.LowestRank  AS  Lowest_Rank__c		
							,case T1.Inactive when 1 then 'TRUE' else 'FALSE' end AS  Inactive__c		 
			 
			 		INTO [hrc_fnc_migration].DBO.IMP_RANKING_ENTITY
 					FROM [hrc_fnc_worknet].dbo.tlkRankEntity AS T1
 
 
 end -- TC1: 14
 
				select * from [hrc_fnc_migration].DBO.IMP_RANKING_ENTITY
  