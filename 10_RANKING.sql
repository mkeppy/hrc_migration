USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tblOrgRank
			WHERE   SF_Object LIKE '%ra%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_RANKING

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,T1.RankID  AS  Rank_ID__c		
							,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,x2.id  as  Ranking_Entity__c		-- Yes/Link(tlkRankEntity.RankEntityID)
							,T1.RankNbr  AS  Rank_Number__c		
							,T1.RankYear  AS  Rank_Year__c		
			 
							,T1.OrgID  as zrefOrgID
							,T1.RankEntityID as zrefRanEntID

			 		 INTO [hrc_fnc_migration].DBO.IMP_RANKING
			
					FROM [hrc_fnc_worknet].dbo.tblOrgRank AS T1
					inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
					left join [hrc_fnc_migration].dbo.xtr_ranking_entity as x2 on t1.[RankEntityID]=x2.Rank_Entity_ID__c
			 

				 
END -- TC1: 21821

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_RANKING
	WHERE Rank_ID__c IN (SELECT Rank_ID__c FROM [hrc_fnc_migration].DBO.IMP_RANKING GROUP BY Rank_ID__c HAVING COUNT(*)>1)
	ORDER BY Rank_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_RANKING
	 
	 
	SELECT * FROM [hrc_fnc_migration].DBO.IMP_RANKING order by [zrefOrgID]

END 