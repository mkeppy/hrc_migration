USE [hrc_fnc_migration]
GO

BEGIN --Generate scripts from map.
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.tblOrgEndorsement
			WHERE   SF_Object LIKE '%end%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM hrc_fnc_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_fnc_migration].DBO.IMP_ENDORSEMENT

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			 
							,T1.OrgEndorsementID  AS  Legacy_ID__c		
					 		,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,T2.[AttributeValue]  AS  Legislative_Item_Endorsed__c	-- Migrate tlkLookupValue.[AttributeValue] via the link.	-- Yes/Link(tlkLookupValue.AttributeValueID)
							,Cast(T1.EndorsementDate  as Date) as  Endorsement_Date__c		
							,T1.Notes  AS  Notes__c		
			 
							,T1.OrgID as zrefOrgId
							,T1.EndorsementID  as zrefEndId
			 		 INTO [hrc_fnc_migration].DBO.IMP_ENDORSEMENT
			
					FROM [hrc_fnc_worknet].dbo.tblOrgEndorsement AS T1
				    left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_fnc_worknet].dbo.[tlkLookupValue] 
								where [AttributeID]='17') as t2 on t1.EndorsementID=t2.[AttributeValueID]
					inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
			 

				 
END -- TC1: 601	

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_ENDORSEMENT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_ENDORSEMENT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c


	SELECT Legislative_Item_Endorsed__c, Len(Legislative_Item_Endorsed__c) l
	from [hrc_fnc_migration].DBO.IMP_ENDORSEMENT
	order by l desc

	
	SELECT Notes__c, Len(Notes__c) l
	from [hrc_fnc_migration].DBO.IMP_ENDORSEMENT
	order by l desc


  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_ENDORSEMENT
	
	select * from [hrc_fnc_migration].DBO.IMP_ENDORSEMENT 
	order by [zrefOrgId]

END 