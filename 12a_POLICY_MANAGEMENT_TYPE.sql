use [hrc_fnc_migration]
go
	

begin 

	DROP TABLE [hrc_fnc_migration].dbo.IMP_POLICY_MANAGEMENT_TYPE

end


begin--- create IMP Policy_Management_Type__c 

						select distinct  [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  					,'PO-'+Cast(T1.[PolicyTypeID]  as Varchar(10)) AS  Legacy_ID__c		
						 		 ,t1.[PolicyType] as [Name]
 					 	into [hrc_fnc_migration].dbo.IMP_POLICY_MANAGEMENT_TYPE
						from [hrc_fnc_worknet].dbo.[tlkPolicyType]  as t1
							
 					union 
	
						select distinct [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 								,'MA-'+Cast(T1.[ManagementID] as Varchar(20)) AS  Legacy_ID__c	
				 				,t1.[ManagementDesc]  as [Name]
						from [hrc_fnc_worknet].dbo.tlkManagementType as t1
				 		 
					union
						 
						select  distinct [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  					,'BE-'+Cast(T1.[BenefitID] as varchar(10)) AS  Legacy_ID__c	
					 		 	,t1.[BenefitDesc] as  [Name] 
 						from [hrc_fnc_worknet].dbo.[tlkBenefit] as t1
					 
					union 
   						select  distinct [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  					,'SP-'+Cast(T1.[SponsorshipTypeID]  as Varchar(10)) as  Legacy_Id__c		
							 	,t1.[SponsorshipTypeDesc]  AS  [Name]
						from [hrc_fnc_worknet].dbo.[tlkSponsorshipType] as t1


end 


begin-- audit

	select * from [hrc_fnc_migration].dbo.IMP_POLICY_MANAGEMENT_TYPE

	select t.legId, Count(*) c 
	from (select   Left ([LEGACY_ID__C], 2) legId, Count(*) c
			from [hrc_fnc_migration].dbo.IMP_POLICY_MANAGEMENT_TYPE
			group by [LEGACY_ID__C]) as t
	group by t.legid

end 