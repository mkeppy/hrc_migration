use [hrc_fnc_migration]
go

--CONGRESSIONAL_SCORECARD__C
 

	select * from [hrc_fnc_maps].dbo.[CHART_Attribute]
	where [SF_Object] ='CONGRESSIONAL_SCORECARD__C'

begin --DELETE tbl
	DROP TABLE [hrc_fnc_migration].DBO.IMP_CONGRESSIONAL_SCORECARD
end

begin--update clean names to match in the IMP below 
		select t1.*, t2.*
		from [hrc_fnc_migration].dbo.tblOrganization_dnc as t1
		inner join [hrc_fnc_migration].dbo.tbl_acct_capitol_name_clean2 as t2  --table manually cleaned up outside of sql.
		  on t1.orgid=t2.zreforgid
		order by t1.OffNameSCLast

		update [hrc_fnc_migration].dbo.tbl_acct_capitol_name_clean2
		set zrefOffNameSCFirst='John' 
		where  zrefOffNameSCLast='Bergman' and zrefOffNameSCFirst='Jack'

		update [hrc_fnc_migration].dbo.tblOrganization_dnc
		set OffNameSCFirst = zrefOffNameSCFirst, OffNameSCLast = zrefOffNameSCLast
		from [hrc_fnc_migration].dbo.tblOrganization_dnc as t1
		inner join [hrc_fnc_migration].dbo.tbl_acct_capitol_name_clean2 as t2  --table manually cleaned up outside of sql.
		  on t1.orgid=t2.zreforgid
end


begin --CREATE tbl 
					select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
						  ,t1.[uniqueid] as Legacy_id__c  
			 			  ,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
								when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
						  ,t1.[Congressional_Session__c]
						  ,t1.[Score__c]
						  
						  
						  --referece
						  ,t1.[OrgId] as zrefOrgId
						  ,T2.[OrgOfficialName] as zrefOrgOfficialName
						  ,T2.OfficialNameScorecard as zrefOfficialNameScorecard	
						  ,T2.OffNameSCFirst as zrefOffNameSCFirst	
						  ,T2.OffNameSCLast as zrefOffNameSCLast
						  ,x2.Name as zrefAcctname
				       into [hrc_fnc_migration].dbo.IMP_CONGRESSIONAL_SCORECARD
					   from [hrc_fnc_migration].dbo.tbl_attributes_cd_final as t1
					   left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
					   left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
/*name matched*/	   left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
					   order by t1.orgId

end   --fnc 4,410. 
	
BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_CONGRESSIONAL_SCORECARD
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_CONGRESSIONAL_SCORECARD GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_CONGRESSIONAL_SCORECARD
	 
	 --missing org ids
	 SELECT distinct zreforgid, zrefOrgOfficialName, zrefOffNameSCFirst ,zrefOffNameSCLast
	 FROM [hrc_fnc_migration].DBO.IMP_CONGRESSIONAL_SCORECARD
	 where [Organization__c] is null 
	 order by zrefOffNameSCLast, zrefOffNameSCFirst

	 
			 
END 
 