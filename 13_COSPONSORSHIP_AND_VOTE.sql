use [hrc_fnc_migration]
go

--Cosponsorships and Votes object 
 

	select * from [hrc_fnc_maps].dbo.[CHART_Attribute]
	where [SF_Object] ='COSPONSORSHIPS_AND_VOTES__c'

begin --DELETE tbl
	DROP TABLE [hrc_fnc_migration].DBO.IMP_COSPONSORSHIP_AND_VOTE
end

begin --CREATE tbl 
			select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
				    ,t1.[uniqueid] as Legacy_id__c
				    ,x3.id as Congressional_Scorecard__c   -- link to congressional scorecard record.
					,case when t1.[Alison_Nathan_Confirmation__c] is null then 'Present' else t1.[Alison_Nathan_Confirmation__c] end as [Alison_Nathan_Confirmation__c]
					,case when t1.[Byrne_E_W_Amendment_HRC_Opposed__c] is null then 'Present' else t1.[Byrne_E_W_Amendment_HRC_Opposed__c] end as [Byrne_E_W_Amendment_HRC_Opposed__c]
					,case when t1.[Co_Sponsor_DOMA__c] is null then 'Present' else t1.[Co_Sponsor_DOMA__c] end as [Co_Sponsor_DOMA__c]
					,case when t1.[Co_Sponsor_DP_Tax__c] is null then 'Present' else t1.[Co_Sponsor_DP_Tax__c] end as [Co_Sponsor_DP_Tax__c]
					,case when t1.[Co_Sponsor_DPBO__c] is null then 'Present' else t1.[Co_Sponsor_DPBO__c] end as [Co_Sponsor_DPBO__c]
					,case when t1.[Co_Sponsor_ENDA__C] is null then 'Present' else t1.[Co_Sponsor_ENDA__C] end as [Co_Sponsor_ENDA__C]
					,case when t1.[Co_Sponsor_Equality_Act__c]  is null then 'Present' else t1.[Co_Sponsor_Equality_Act__c]  end as [Co_Sponsor_Equality_Act__c] 
					,case when t1.[Co_Sponsor_FADA_HRC_Opposed__c] is null then 'Present' else t1.[Co_Sponsor_FADA_HRC_Opposed__c] end as [Co_Sponsor_FADA_HRC_Opposed__c]
					,case when t1.[Co_Sponsor_FMA_HRC_Opposed__c] is null then 'Present' else t1.[Co_Sponsor_FMA_HRC_Opposed__c] end as [Co_Sponsor_FMA_HRC_Opposed__c]
					,case when t1.[Co_Sponsor_GRA__C] is null then 'Present' else t1.[Co_Sponsor_GRA__C] end as [Co_Sponsor_GRA__C]
					,case when t1.[Co_Sponsor_RMA__C] is null then 'Present' else t1.[Co_Sponsor_RMA__C] end as [Co_Sponsor_RMA__C]
					,case when t1.[Co_Sponsor_SNDA__c] is null then 'Present' else t1.[Co_Sponsor_SNDA__c] end as [Co_Sponsor_SNDA__c]
					,case when t1.[Co_Sponsor_SSIA__c] is null then 'Present' else t1.[Co_Sponsor_SSIA__c] end as [Co_Sponsor_SSIA__c]
					,case when t1.[Co_Sponsor_UAFA__c] is null then 'Present' else t1.[Co_Sponsor_UAFA__c] end as [Co_Sponsor_UAFA__c]
					,case when t1.[DADT_Repeal__c] is null then 'Present' else t1.[DADT_Repeal__c] end as [DADT_Repeal__c]
					,case when t1.[DOMA__C] is null then 'Present' else t1.[DOMA__C] end as [DOMA__C]
					,case when t1.[ENDA__c] is null then 'Present' else t1.[ENDA__c] end as [ENDA__c]
					,case when t1.[FMA_HRC_Opposed__c] is null then 'Present' else t1.[FMA_HRC_Opposed__c] end as [FMA_HRC_Opposed__c]
					,case when t1.[Foxx_Amdt_DOD_HRC_Opposed__c] is null then 'Present' else t1.[Foxx_Amdt_DOD_HRC_Opposed__c] end as [Foxx_Amdt_DOD_HRC_Opposed__c]
					,case when t1.[Hartzler_Amendment_HRC_OPPOSED__c] is null then 'Present' else t1.[Hartzler_Amendment_HRC_OPPOSED__c] end as [Hartzler_Amendment_HRC_OPPOSED__c]
					,case when t1.[Hate_Crimes__c] is null then 'Present' else t1.[Hate_Crimes__c] end as [Hate_Crimes__c]
					,case when t1.[House_VAWA_HRC_Opposed__c] is null then 'Present' else t1.[House_VAWA_HRC_Opposed__c] end as [House_VAWA_HRC_Opposed__c]
					,case when t1.[Huelskamp_Amdt_CJS_HRC_Opposed__c] is null then 'Present' else t1.[Huelskamp_Amdt_CJS_HRC_Opposed__c] end as [Huelskamp_Amdt_CJS_HRC_Opposed__c]
					,case when t1.[Huelskamp_Amdt_DOD_HRC_Opposed__c] is null then 'Present' else t1.[Huelskamp_Amdt_DOD_HRC_Opposed__c] end as [Huelskamp_Amdt_DOD_HRC_Opposed__c]
					,case when t1.[Hutchinson_Amdt_VAWA_HRC_Opposed__c] is null then 'Present' else t1.[Hutchinson_Amdt_VAWA_HRC_Opposed__c] end as [Hutchinson_Amdt_VAWA_HRC_Opposed__c]
					,case when t1.[King_Amdt_DOD_HRC_Opposed__c] is null then 'Present' else t1.[King_Amdt_DOD_HRC_Opposed__c] end as [King_Amdt_DOD_HRC_Opposed__c]
					,case when t1.[Loretta_Lynch_Cloture__c] is null then 'Present' else t1.[Loretta_Lynch_Cloture__c] end as [Loretta_Lynch_Cloture__c]
					,case when t1.[Loretta_Lynch_Confirmation__c] is null then 'Present' else t1.[Loretta_Lynch_Confirmation__c] end as [Loretta_Lynch_Confirmation__c]
					,case when t1.[Maloney_E_W_Amendment__c] is null then 'Present' else t1.[Maloney_E_W_Amendment__c] end as [Maloney_E_W_Amendment__c]
					,case when t1.[Maloney_Milcon_VA_Amendment__c] is null then 'Present' else t1.[Maloney_Milcon_VA_Amendment__c] end as [Maloney_Milcon_VA_Amendment__c]
					,case when t1.[MCAA_Shaheen_Amendment__c] is null then 'Present' else t1.[MCAA_Shaheen_Amendment__c] end as [MCAA_Shaheen_Amendment__c]
					,case when t1.[Oppose_DADT_Creation__c] is null then 'Present' else t1.[Oppose_DADT_Creation__c] end as [Oppose_DADT_Creation__c]
					,case when t1.[Paul_Oetken_Confirmation__c] is null then 'Present' else t1.[Paul_Oetken_Confirmation__c] end as [Paul_Oetken_Confirmation__c]
					,case when t1.[Pittenger_E_W_Amendment_HRC_Opposed__c] is null then 'Present' else t1.[Pittenger_E_W_Amendment_HRC_Opposed__c] end as [Pittenger_E_W_Amendment_HRC_Opposed__c]
					,case when t1.[RHYTPA_Leahy_Collins_Amendment__c] is null then 'Present' else t1.[RHYTPA_Leahy_Collins_Amendment__c] end as [RHYTPA_Leahy_Collins_Amendment__c]
					,case when t1.[Schatz_Murray_Amendment__c] is null then 'Present' else t1.[Schatz_Murray_Amendment__c] end as [Schatz_Murray_Amendment__c]
					,case when t1.[Senate_ENDA_1st_Cloture__c] is null then 'Present' else t1.[Senate_ENDA_1st_Cloture__c] end as [Senate_ENDA_1st_Cloture__c]
					,case when t1.[Senate_Enda_2nd_Cloture__c] is null then 'Present' else t1.[Senate_Enda_2nd_Cloture__c] end as [Senate_Enda_2nd_Cloture__c]
					,case when t1.[Senate_ENDA_Final_Passage__c] is null then 'Present' else t1.[Senate_ENDA_Final_Passage__c] end as [Senate_ENDA_Final_Passage__c]
					,case when t1.[Senate_Grassley_Amdt_to_VAWA_HRC_Oppo__c] is null then 'Present' else t1.[Senate_Grassley_Amdt_to_VAWA_HRC_Oppo__c] end as [Senate_Grassley_Amdt_to_VAWA_HRC_Oppo__c]
					,case when t1.[Senate_VAWA__c] is null then 'Present' else t1.[Senate_VAWA__c] end as [Senate_VAWA__c]
					,case when t1.[SNDA_Esea_Franken_Amendment__c] is null then 'Present' else t1.[SNDA_Esea_Franken_Amendment__c] end as [SNDA_Esea_Franken_Amendment__c]
					,case when t1.[THUD_Peters_Amendment__c] is null then 'Present' else t1.[THUD_Peters_Amendment__c] end as [THUD_Peters_Amendment__c]

				    ,t1.[OrgId] as zrefOrgId
	 		 	 into [hrc_fnc_migration].dbo.IMP_COSPONSORSHIP_AND_VOTE
			   from [hrc_fnc_migration].dbo.tbl_attributes_covote_final as t1
			   inner join [hrc_fnc_migration].dbo.IMP_CONGRESSIONAL_SCORECARD as t3 on t1.[uniqueid] = t3.[Legacy_id__c]
		 	   left join [hrc_fnc_migration].dbo.xtr_congressional_scorecard as x3 on t3.[Legacy_id__c]=x3.[Legacy_id__c]
			   order by t1.orgId
end  
 

	
BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_COSPONSORSHIP_AND_VOTE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_COSPONSORSHIP_AND_VOTE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_fnc_migration].DBO.IMP_COSPONSORSHIP_AND_VOTE
	  
	select * from  [hrc_fnc_migration].DBO.IMP_COSPONSORSHIP_AND_VOTE
	order by congressional_scorecard__c
			 
END 

 