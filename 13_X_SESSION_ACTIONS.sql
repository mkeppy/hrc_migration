
 /*
		X103rd_Session_Actions__c
		X104th_Session_Actions__c
		X108th_Session_Actions__c
		X109th_Session_Actions__c
		x110th_Session_Actions__c
		x111th_Session_Actions__c
		x112th_Session_Actions__c
		x113th_Session_Actions__c
		x114th_Session_Actions__c
		x115th_Session_Actions__c

*/

-- X103rd_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.Oppose_DADT_Creation__c, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		-- into [hrc_fnc_migration].dbo.IMP_X103rd_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where t1.refAttributeValueID='1899'  
		--102
		
--X104th_Session_Actions__c

		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.DOMA__C, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
	--	 into [hrc_fnc_migration].dbo.IMP_X104th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		c
		--125
				--KW account does not exist, and acct was exclude from conversion. 
				select * from [hrc_fnc_worknet].dbo.[tblOrganization] where orgid='62229'
				select * from [hrc_fnc_migration].dbo.tblOrganization_dnc where orgid='62229'
				select * from [hrc_fnc_migration].dbo.xtr_account where name like '%fattah%'

--X108th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.FMA_HRC_Opposed__c, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		-- into [hrc_fnc_migration].dbo.IMP_X108th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where refAttributeValueID='1707'
		--295
 

--X109th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.FMA_HRC_Opposed__c, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		 --into [hrc_fnc_migration].dbo.IMP_X109th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where  refAttributeValueID='1708'
		--329


--X110th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.ENDA__c, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		-- into [hrc_fnc_migration].dbo.IMP_X110th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where refAttributeValueID='1894'
		--241

--X111th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.DADT_Repeal__c
			,t1.Hate_Crimes__c

			
			,t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		-- into [hrc_fnc_migration].dbo.IMP_X111th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where refAttributeValueID='1896' 
		--358

--X112th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 			,t1.Alison_Nathan_Confirmation__c
			,t1.Co_Sponsor_DP_Tax__c
			,t1.Co_Sponsor_DPBO__c
			,t1.Co_Sponsor_ENDA__C
			,t1.Co_Sponsor_RMA__C
			,t1.Co_Sponsor_UAFA__c
			,t1.Foxx_Amdt_DOD_HRC_Opposed__c
			,t1.House_VAWA_HRC_Opposed__c
			,t1.Huelskamp_Amdt_CJS_HRC_Opposed__c
			,t1.Huelskamp_Amdt_DOD_HRC_Opposed__c
			,t1.Hutchinson_Amdt_VAWA_HRC_Opposed__c
			,t1.King_Amdt_DOD_HRC_Opposed__c
			,t1.Paul_Oetken_Confirmation__c
			,t1.Senate_VAWA__c
 
			,t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		-- into [hrc_fnc_migration].dbo.IMP_X112th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where t1.refAttributeValueId ='1818' 
		--545



--X113th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			 ,t1.Co_Sponsor_ENDA__C  
			 ,t1.Co_Sponsor_FMA_HRC_Opposed__c  
			,t1.Co_Sponsor_RMA__C  
			,t1.Co_Sponsor_SNDA__C  
			,t1.Co_Sponsor_SSIA__c  
			,t1.Senate_ENDA_1st_Cloture__c  
			,t1.Senate_Enda_2nd_Cloture__c  
			,t1.Senate_ENDA_Final_Passage__c  
			,t1.Senate_Grassley_Amdt_to_VAWA_HRC_Oppo__c   
 
			
			,t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		 into [hrc_fnc_migration].dbo.IMP_X113th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where t1.refAttributeValueID ='1922'
		 --550

--X114th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
				,t1.Byrne_E_W_Amendment_HRC_Opposed__c
				,t1.Co_Sponsor_Equality_Act__c
				,t1.Co_Sponsor_FADA_HRC_Opposed__c
				,t1.Co_Sponsor_GRA__C
				,t1.Co_Sponsor_SNDA__C
				,t1.Co_Sponsor_SSIA__C
				,t1.Loretta_Lynch_Cloture__c
				,t1.Loretta_Lynch_Confirmation__c
				,t1.Maloney_E_W_Amendment__c
				,t1.Maloney_Milcon_VA_Amendment__c
				,t1.MCAA_Shaheen_Amendment__c
				,t1.Pittenger_E_W_Amendment_HRC_Opposed__c
				,t1.RHYTPA_Leahy_Collins_Amendment__c
				,t1.Schatz_Murray_Amendment__c
				,t1.SNDA_Esea_Franken_Amendment__c
				,t1.THUD_Peters_Amendment__c

			
			,t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		 into [hrc_fnc_migration].dbo.IMP_X114th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where t1.refAttributeValueID ='2289'
		--545


--X115th_Session_Actions__c
		select [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId
			,t1.[uniqueid] as Legacy_id__c  
			,case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
			 when x2.id is not null then x2.id else  x1.id  end as  Organization__c		-- Yes/Link(tblOrganization.OrgID)
 	
			,t1.Hartzler_Amendment_HRC_OPPOSED__c, 
			
			t1.[OrgId] as zrefOrgId, t1.[uniqueid] as zrefUniqueId
		 into [hrc_fnc_migration].dbo.IMP_X115th_Session_Actions__c
		from  [hrc_fnc_migration].dbo.tbl_attributes_x_obj_final as t1
		left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
		left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t1].[OrgId] = [t2].[OrgID]
		left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 		where Hartzler_Amendment_HRC_OPPOSED__c is not null   
		--434
 	

			