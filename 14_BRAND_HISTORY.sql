

use [hrc_fnc_migration]
go


begin-- BRAND_HISTORY
					select  distinct 
							[hrc_fnc_migration].[dbo].[fnc_OwnerId]()   AS OwnerId 
							,t.HistoryID as Legacy_Id__c
							,t.Category as Category__c
							,t.CEIRatingWeb as CEI_Rating_Web__c
							,t.OrgCommonName as Common_Name__c
							,t.BrandName as [Name]
 							,x1.id as Organization__c
 							,t.HistoryYear as Year__c
				 	into [hrc_fnc_migration].dbo.IMP_BRAND_HISTORY
					from [hrc_fnc_worknet].dbo.[tblOrgBrandHistory] as t 
					inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t.[OrgID] as Varchar(20))=x1.legacy_id__c

					--2185

end 

BEGIN -- AUDIT


	SELECT * FROM [hrc_fnc_migration].DBO.IMP_BRAND_HISTORY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_fnc_migration].DBO.IMP_BRAND_HISTORY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	 


  	SELECT * FROM [hrc_fnc_migration].DBO.IMP_BRAND_HISTORY order by [Organization__c]
	
	 

END 					 