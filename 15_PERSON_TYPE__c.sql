

		select * from [hrc_delta_migration].dbo.[xtr_record_type] where [SOBJECTTYPE]='Person_Type__c'
		'0125A000001QCFFQA4	HEI					Person_Type__c
		 0125A000001QCFKQA4	CEI					Person_Type__c
		 0125A000001QCFPQA4	ACAF				Person_Type__c
		 0125A000001QCFUQA4	Government Affairs	Person_Type__c'


		DROP TABLE [hrc_delta_migration].dbo.IMP_PERSON_TYPE


		select	[hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId,
				t.[PersonTypeID] as Legacy_Id__c,
				x1.id Contact__c, 
				l.[AttributeValue] Person_Type__c, 
				case when x3.id is null then '0125A000001QCFUQA4' else x3.id end as RecordTypeId,   --Government Affairs	Person_Type__c
				---case when x2.[NAME] 
				
				x2.[NAME] as zContactRecordType, x2.id zContactRecordTypeId,
				x3.[NAME] as zPersonTypeRecType, x3.id zPersonTypeRecTypeId,
				t.[PersonID] as zrefPersonId, 
				x2.[NAME] zContRecdType

		 into [hrc_delta_migration].dbo.IMP_PERSON_TYPE
		 from [hrc_delta_worknet].dbo.[tblPersonType]  T
		 inner join [hrc_fnc_maps].dbo.chart_persontype c2 on t.[TypeID]=c2.attributevalueid
		 inner join  [hrc_delta_worknet].dbo.[tlkLookupValue]  l on [T].[TypeID] = [l].[AttributeValueID]
		 inner join [hrc_delta_migration].dbo.xtr_contact x1 on t.[PersonID]=x1.[LEGACY_ID__C]
		 left join [hrc_delta_migration].dbo.[xtr_record_type] x2 on x1.[RECORDTYPEID]=x2.[ID]  --contact record type
		 left join (select * from [hrc_delta_migration].dbo.[xtr_record_type] where [SOBJECTTYPE]='Person_Type__c') as x3 on x2.name=x3.name  --person type recprd type

		 where c2.[Convert]='Yes'
	  	 order BY x1.id, l.[AttributeValue]
			--125,211 all
			--106727  convert=yes

		
		select * from [hrc_delta_migration].dbo.IMP_PERSON_TYPE 
		order by contact__C

			select [AttributeValueID], [AttributeID], [AttributeValue] 
			from [hrc_delta_worknet].dbo.[tlkLookupValue] 
			where [AttributeID]='3'   
			order by [AttributeValueID] 