	--FB-00108

	 

		 select [Organization__c], [zrefOrgId], [Survey_Response_ID__c], [Most_Current__c], [Legacy_ID__c] 
		 from [hrc_fnc_migration].DBO.IMP_POLICIES_AND_MANAGEMENT where [zrefOrgId]='55252' order by [Survey_Response_ID__c], [Most_Current__c]


		 select [Organization__c]+Question__c+[Most_Current__c], Count(*) c, [Organization__c]
		from [hrc_fnc_migration].DBO.IMP_POLICIES_AND_MANAGEMENT
		where [Most_Current__c]='true'
		group by [Organization__c]+Question__c+[Most_Current__c], [Organization__c]
		having Count(*)>1
		order by c desc

		 select distinct [Organization__c], Question__c,   [Most_Current__c]
		 from [hrc_fnc_migration].DBO.IMP_POLICIES_AND_MANAGEMENT 
		 where [Organization__c]+Question__c+[Most_Current__c] in (select [Organization__c]+Question__c+[Most_Current__c]
																				 from [hrc_fnc_migration].DBO.IMP_POLICIES_AND_MANAGEMENT
																				 group by [Organization__c]+Question__c+[Most_Current__c]
																			 having Count(*)>1)
		and [Most_Current__c] ='true'
		order by [Organization__c] , [Survey_Response_ID__c],[Most_Current__c]

							select   distinct [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  					,'OP-'+Cast(T1.OrgPolicyID  as Varchar(10)) AS  Legacy_ID__c		
						 		,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
								,x2.id  AS  Survey_Response_ID__c	-- Link to the SurveyResponse record created on tblOrgSurveyResponse that matches [OrgID] and [SurveyYear]	
								,x3.id  AS  Question__c	-- Migrate tlkPolicyType.[PolicyType]  value via the link.	-- Yes/Link(tlkPolicyType.PolicyTypeID)
								,T1.MostCurrentInd
								,T1.Included  AS  Response__c		
								,T1.GlobalIncluded  AS  Global_Response__c		
								,T1.Extent  AS  Sub_Response__c		
								,T1.LanguageUsed  AS  Additional_Information__c		
								,null as Applies_To__c --filler
								,Cast(T1.EffectiveDate as Date) AS  Effective_Date__c
								,null as Notes__c --filler	
								,t3.[FirstName] + ' ' + t3.[LastName]    AS  Legacy_Last_Updated_By__c	-- Migrate FirstName + " " + LastName from tlkStaff via the link.	-- Yes/Link(tlkStaff.StaffID)
								,Cast(T1.DateEntered  as Date) as  Date_Entered__c	
								,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as Survey_Year__c		
								,case T1.MostCurrentInd when 1 then 'TRUE' else 'FALSE' end   AS  Most_Current__c		
  								,'0120m0000004WrNAAU' AS  RecordTypeId	-- All data from this table migrates with Record Type = "Policy"	 
						 
								--reference
								,T1.OrgID as zrefOrgId
								,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as zrefSurveyYear
								,'tblOrgPolicy' zrefSrc
								 
								,t4.legacy_id__c  as zrefSurverResponseLegacyId
 							from [hrc_fnc_worknet].dbo.tblOrgPolicy as t1
						 	inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_fnc_migration].dbo.IMP_SURVEY_RESPONSE as t4 on (case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end)=t4.Survey_Year__c and t1.[OrgID]=t4.zrefOrgId
					 		left join [hrc_fnc_migration].dbo.xtr_survey_response as x2 on t4.Legacy_Id__c=x2.Legacy_Id__c
							left join [hrc_fnc_worknet].dbo.[tlkPolicyType] as t2 on t1.[PolicyTypeID]=t2.[PolicyTypeID]
							left join [hrc_fnc_worknet].dbo.[tlkStaff] as t3 on [T1].LastUpdatedByStaffID = [t3].[StaffID]
  							left join (select * from [hrc_fnc_migration].dbo.xtr_policy_management_type where legacy_id__c like 'PO%') as x3 on 'PO-'+Cast(t2.[PolicyTypeID] as Varchar(10))=x3.legacy_id__c							
 							where t1.[OrgID]='55252'

							select * from [hrc_fnc_worknet].dbo.tblOrgPolicy where [OrgID]='55252'
							select * from [hrc_fnc_migration].dbo.IMP_SURVEY_RESPONSE 



							
 --FB-00107
						select * from [hrc_fnc_maps].dbo.chart_persontype
						where [AttributeValue] like '%2018%'

							2328	3	CEI - 2018 Survey CONTRIBUTOR
							2329	3	CEI - 2018 Survey OFFICIAL SUBMITTER
							2377	3	HEI - 2018 Survey Official Submitter
							2378	3	HEI - 2018 Press Contact
							2379	3	HEI - 2018 Survey Contributor

				select * from [hrc_delta_worknet].[dbo].[qrySubPersonType]
				where [PersonID] in (select [PersonID] from [hrc_delta_worknet].[dbo].[qrySubPersonType] group by [PersonID] having Count(*)>1)
				order by [PersonID], [DisplayOrder]

		--xport to xcel and run update. 
		select distinct x.id, t.[PersonID] zrefPersonId,  
			Stuff(( select ' '+c.AttributeValue+';'
					from [hrc_delta_worknet].dbo.[tblPersonType]  as t1
					inner join [hrc_fnc_maps].dbo.chart_persontype c on t1.[TypeID]=c.attributevalueid
						where T1.[PersonID]=T.[PersonID]  and c.[convert]='Yes'  
						order by T1.[PersonID] 
					for
						xml path('')
							), 1, 1, '') as Person_Type__c
	     
		from [hrc_delta_worknet].dbo.[tblPersonType]  T
		 inner join [hrc_fnc_maps].dbo.chart_persontype c2 on t.[TypeID]=c2.attributevalueid
		 inner join [hrc_delta_migration].dbo.xtr_contact x on t.[PersonID]=x.[LEGACY_ID__C]
		left join [hrc_fnc_migration].dbo.[tbl_person_type] f on [T].[PersonID] = [f].[PersonID]
	 	 where c2.[Convert]='Yes'    
	  	order BY t.[PersonID] desc





		select * from [hrc_delta_migration].dbo.xtr_contact
		select * from [hrc_delta_migration].dbo.tbl_person_type  where personid='61006'


		select [Legacy_Id__c], [Person_type__c], [FirstName], [LastName] 
		from [hrc_delta_migration].dbo.[IMP_DELTA_CONTACT]
		 where [Person_type__c] like '%2018%'
	--fb-00103
				
			--attribute	
				select distinct  t.[OrgID], t.[AttributeID], t.[Description]
		 		from [hrc_fnc_worknet].dbo.[tblAttribute] t
				inner join [hrc_fnc_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
				where t.[AttributeID]='2387'

			--type
				select distinct t.*
 				from [hrc_fnc_worknet].dbo.[tblPersonType] as t
				where t.typeid='2387' 


	--fb-00090 review 
				select x.id, x.[ACCOUNTID], x.[NAME], x.[LEGACY_ID__C], t.[PersonID], t.[TypeID]
				from [hrc_fnc_migration].dbo.[xtr_contact] x
				inner join (select distinct t.[PersonID], t.[TypeID]
 							from [hrc_fnc_worknet].dbo.[tblPersonType] as t
							where t.[TypeID]='1275') t on x.[LEGACY_ID__C]=t.[PersonId]
	 	
		
		select distinct t.*
 		from [hrc_fnc_worknet].dbo.[tblPersonType] as t
		where t.[PersonID] in (select d.personId from [hrc_fnc_migration].dbo.tbl_person_dnc d where d.[TypeID]='1275')
		
		select d.*, x.*
		from [hrc_fnc_migration].dbo.tbl_person_dnc d 
		left join 	[hrc_fnc_migration].dbo.[xtr_contact] x on d.[PersonID]=x.[LEGACY_ID__C]				
		where d.[PersonID] in (select  t.[PersonID] 
 							from [hrc_fnc_worknet].dbo.[tblPersonType] as t
							where t.[TypeID]='1275')

		select  t.*
 		from [hrc_fnc_worknet].dbo.[tblPersonType] as t
		where t.[PersonID]='74736'


		--- KW Contacts. fix. match Access contact to KW Contats by email address. Update PersonType on the KW contact and delete the Access contact created. 

				select * 
				into [hrc_fnc_migration].dbo.tbl_kw_contacts
				from [hrc_fnc_migration].dbo.[xtr_contact]
				where [CREATEDBYID] ='0055A000006H7a9QAC'


				select * 
				into [hrc_fnc_migration].dbo.tbl_contacts_lgbt_staffer_1275
				from [hrc_fnc_migration].dbo.imp_contact
				where [Person_type__c] like '%LEG - LGBT Staffer%'

	
				select x2.id, x2.[NAME], x1.id, t1.[Legacy_Id__c], t1.[FirstName], t1.[LastName], t1.[Email], t1.[Person_type__c], 
				t2.id as kw_id, t2.[FIRSTNAME] as kw_firstname, t2.[LASTNAME] as kw_lastname, t2.[EMAIL] as kw_email, t2.[CREATEDBYID]kw_createdby
				from [hrc_fnc_migration].dbo.tbl_contacts_lgbt_staffer_1275 as t1
				inner join [hrc_fnc_migration].dbo.tbl_kw_contacts as t2 on t1.[Email]=t2.[EMAIL]
				left join [hrc_fnc_migration].dbo.[xtr_contact] as x1 on t1.[Legacy_Id__c]=x1.[LEGACY_ID__C]
				left join [hrc_fnc_migration].dbo.xtr_account as x2 on x1.[ACCOUNTID]=x2.id
				order by t1.[LastName], t1.[FirstName]



	---fb-000086
	
				select x.id, x.legacy_id__c, x.[name], t.[AttributeID], t.[Description]
				from [hrc_fnc_migration].dbo.[xtr_account]  x
				inner join (select distinct  t.[OrgID], t.[AttributeID], t.[Description]
		 					from [hrc_delta_worknet].dbo.[tblAttribute] t
							inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
							where t.[AttributeID]='241') t on x.legacy_id__c=Cast(t.[OrgID] as Varchar(20))
				order by t.[Description] desc, t.[OrgID]

	 



	 --FB-00013 
			--dnc account
				select distinct  t.[OrgID]
		 		from [hrc_fnc_worknet].dbo.[tblAttribute] t
				inner join [hrc_fnc_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
				where t.[AttributeID]='2382'
		 
				select x.id, x.legacy_id__c, x.[name], t.[AttributeID]
				from [hrc_fnc_migration].dbo.[xtr_account]  x
				inner join (select distinct  t.[OrgID], t.[AttributeID]
		 					from [hrc_fnc_worknet].dbo.[tblAttribute] t
							inner join [hrc_fnc_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
							where t.[AttributeID]='2382') t on x.legacy_id__c=Cast(t.[OrgID] as Varchar(20))
				order by t.[OrgID]

 	
			--dnc person
				select distinct t.[PersonID], t.[TypeID]
 				from [hrc_fnc_worknet].dbo.[tblPersonType] as t
				where t.[TypeID]='2383'
			  

				select x.id, x.[ACCOUNTID], x.[NAME], x.[LEGACY_ID__C], t.[PersonID], t.[TypeID]
				from [hrc_fnc_migration].dbo.[xtr_contact] x
				inner join (select distinct t.[PersonID], t.[TypeID]
 							from [hrc_fnc_worknet].dbo.[tblPersonType] as t
							where t.[TypeID]='2383') t on x.[LEGACY_ID__C]=t.[PersonId]
		
				  
			 

	 --
	 select * from [hrc_fnc_migration].dbo.[IMP_ACCOUNT] where [Legacy_Id__c]='ATTR-3820' -- 0015A00001yWBRcQAO
	 select * from [hrc_fnc_migration].dbo.[IMP_ACCOUNT_parent] where [Parentid]='0015A00001yWBRcQAO'
		
		
							select 
							case when t2.[OrgOfficialName] like '%former%' then '0015A00001yWKoOQAW' --Account "Former US Congressional Members" 
							 when x2.id is not null then x2.id else  x1.id  end as  ID	
						

							,t8.LGBT_Friendly__C
							,t8.Lobbyist_Region__c
						 	
							,x1.id zrefAcct
							,x2.id zrefAcct_capoff
							--reference
							,[t8].[OrgId] as zrefOrgId
							from   [hrc_fnc_migration].dbo.tbl_attributes_account_1_final as t8  
							left join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t8.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_fnc_migration].dbo.tblOrganization_dnc t2 on [t8].[OrgId] = [t2].[OrgID]
							left join [hrc_fnc_migration].dbo.xtr_account as x2 on x2.[name] like 'Capitol office%'+'%'+ t2.OffNameSCFirst+'%'+t2.OffNameSCLast+'%'
 							where (t8.LGBT_Friendly__C is not null or t8.Lobbyist_Region__c is not null )
							and (x2.id is not null or x1.id is not null )

	
	
	--FB-00073
	
	 select distinct Healthcare_System__c from [hrc_fnc_migration].dbo.[tbl_attributes_account_1_final]
	 where [Healthcare_System__c] is not null and Healthcare_System__c!='N/A' and [Healthcare_System__c]!='Not part of network/system'

	 select * from [hrc_fnc_worknet].dbo.[tblAttribute] 
	 where  [AttributeID]='692'  and [Description] like '%HealthAlliance of the Hudson Valley'
	 order by [Description]

	 update [hrc_fnc_worknet].dbo.[tblAttribute]  
	 set [Description]='HealthAlliance of the Hudson Valley'
	 where [AttributeID]='692' and [Description] like '%HealthAlliance of the Hudson Valley'

	 select [OrgID],   [Description]   , 'ATTR-'+Cast(rank() over (order by [Description]) + 1000 as Varchar(20)) as seq
	 ,rank() over (partition by [Description] order by [Description]) seq2
	 from [hrc_fnc_worknet].dbo.[tblAttribute] 
	 where [AttributeID]='692'  and [Description]!='N/A'
	-- group by [Description]
	 order by [Name]

	 select [Description]
	 from [hrc_fnc_worknet].dbo.[tblAttribute] 
	 where [AttributeID]='692'  and [Description]!='N/A'
	 order by [Description]



	 select t.[OrgID], t.[OrgOfficialName], t.[ParentOrgID], t2.[OrgOfficialName] parent_name 
	 from [hrc_fnc_worknet].dbo.[tblOrganization] t
	 left join [hrc_fnc_worknet].dbo.[tblOrganization] t2 on t.[ParentOrgID]=t2.[OrgID]
	  left join [hrc_fnc_worknet].dbo.[tblOrganization] t3 on t.[HEIOrigOrgID]=t3.[OrgID]
	  where t.[ParentOrgID] is not null or t.[HEIOrigOrgID] is not null

	--FB--00069
					select	Cast(T1.[OrgID] as Varchar(10)) +'-'+Cast( T1.[SurveyYear] as Varchar(10)) +'-'+Cast( T1.[SurveyID]as Varchar(10))  AS  Legacy_Id__c		
						 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
						 	,x2.id  AS  Program_Survey__c	-- Link to the Program__c record created from the tblOrgProgram table	-- Yes/Link(tblOrgProgram.ProgramID)
							,Min(Cast(T1.SurveyDateSent  as Date)) as  Date_Sent__c		
							,Max(Cast(T1.SurveyResponseDate as Date)) AS  Response_Date__c		
							,T1.SurveyYear  AS  Survey_Year__c		
							,Max(T1.Notes)  AS  Notes__c		
			  
	 				 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then t3.OfficialSubmitterSignature end as Official_Submitter_Signature__c
					 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then Cast(t3.[OfficialSubmitterSignatureDate] as Date) end as Official_Submitter_Signature_Date__c
							,case when t6.HEI_Leader_Survey_Respondent__c like '%'+Cast(T1.SurveyYear as Varchar(30))+'%' then 'true' else 'false' end test 
							 
							,t6.HEI_Leader_Survey_Respondent__c
							,T1.OrgID  as zrefOrgId
							,T1.SurveyID as zrefSurveyIDProgId
							,T1.SurveyYear as zrefSurveyYear
							,t2.zrefProgramId
			 				,t3.[OfficialSubmitterID]
							,t5.[ContributorID]
							,t4.[PersonID]
							,t4.[FirstName] , t4.[LastName]
							
			 		from [hrc_fnc_worknet].dbo.tblOrgSurveyResponse as T1
				 	inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20)) =x1.Legacy_id__c
					left join [hrc_fnc_migration].dbo.imp_program as t2 on t1.SurveyID=t2.zrefProgramId
				 	left join [hrc_fnc_migration].dbo.xtr_program as x2 on t2.[Name]= x2.[Name]
	 			 	left join [hrc_fnc_worknet].dbo.[tblCEIOrganization] as t3 on [T1].[OrgID] = [t3].[OrgID] and [T1].[SurveyID] = [t3].[SurveyID] and t1.[SurveyID]=t2.zrefPRogramId
					
					left join [hrc_fnc_worknet].dbo.tblCEIOrganizationContributor  as t5 on t3.[OfficialSubmitterID]=t5.[ContributorID]
					left join [hrc_fnc_worknet].dbo.[tblPerson] as t4 on t5.[PersonID]=t4.[PersonID]
					left join [hrc_fnc_migration].dbo.tbl_attributes_account_1_multiselect  as t6 on [T1].[OrgID] = [t6].[OrgID]
 	                where t6.HEI_Leader_Survey_Respondent__c is not null 
				  --   where t1.[OrgID]='61479'
					group by  t1.[SurveyYear],  t1.[OrgID], t1.[SurveyID],t2.zrefProgramId, 
					t3.OfficialSubmitterSignatureDate, t3.OfficialSubmitterSignature , x1.id, x2.id, t3.[OfficialSubmitterID],t4.[FirstName] , t4.[LastName]
					, t4.[PersonID], 		t5.[ContributorID],  t6.HEI_Leader_Survey_Respondent__c



					select * from [hrc_fnc_worknet].dbo.[tblCEIOrganization] where OfficialSubmitterID='18936'
					select * from [hrc_fnc_worknet].dbo.[tblPerson] where [LastName]='Castaneda' and [FirstName]='carlos'
					select * from [hrc_fnc_worknet].dbo.[tblCEIOrganizationContributor] where [ContributorLastName]='castaneda'

	--FB--00052
	
							select t1.[OrgOfficialName], t6.*
							from [hrc_fnc_worknet].dbo.tblOrganization AS T1 
							inner join [hrc_fnc_migration].dbo.tblOrganization_final t on t1.[OrgID]=t.orgid 
							left join [hrc_fnc_migration].dbo.tbl_account_industry as t6 on t1.[OrgID]=t6.orgid
							 left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_fnc_worknet].dbo.[tlkLookupValue] 
								where [AttributeID]='25') as t7 on t1.[OrigSourceID]=t7.[AttributeValueID]

							where t1.[OrgID]='55480'
					

							select x.id, t.* from [hrc_fnc_migration].dbo.tbl_account_industry t
							inner join [hrc_fnc_migration].dbo.[xtr_account] x on Cast(t.[OrgID] as Varchar(30))=x.[LEGACY_ID__C]
							where [OrgID]='55480'

							select * from [hrc_fnc_worknet].dbo.[tblOrgIndustry] where [OrgID]='55480'
	
	--FB-00025
	
	
					SELECT   [hrc_fnc_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 						  	,Cast(T1.[OrgID] as Varchar(10)) +'-'+Cast( T1.[SurveyYear] as Varchar(10)) +'-'+Cast( T1.[SurveyID]as Varchar(10))  AS  Legacy_Id__c		
						 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
						 	,x2.id  AS  Program_Survey__c	-- Link to the Program__c record created from the tblOrgProgram table	-- Yes/Link(tblOrgProgram.ProgramID)
							,Min(Cast(T1.SurveyDateSent  as Date)) as  Date_Sent__c		
							,Max(Cast(T1.SurveyResponseDate as Date)) AS  Response_Date__c		
							,T1.SurveyYear  AS  Survey_Year__c		
							,Max(T1.Notes)  AS  Notes__c		
			  
	 				 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then t3.OfficialSubmitterSignature end as Official_Submitter_Signature__c
					 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then Cast(t3.[OfficialSubmitterSignatureDate] as Date) end as Official_Submitter_Signature_Date__c

							,T1.OrgID  as zrefOrgId
							,T1.SurveyID as zrefSurveyIDProgId
							,T1.SurveyYear as zrefSurveyYear
							,t2.zrefProgramId
			 		
				 	from [hrc_fnc_worknet].dbo.tblOrgSurveyResponse as T1
				 	inner join [hrc_fnc_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20)) =x1.Legacy_id__c
					left join [hrc_fnc_migration].dbo.imp_program as t2 on t1.SurveyID=t2.zrefProgramId
				 	left join [hrc_fnc_migration].dbo.xtr_program as x2 on t2.[Name]= x2.[Name]
	 			 	left join [hrc_fnc_worknet].dbo.[tblCEIOrganization] as t3 on [T1].[OrgID] = [t3].[OrgID] and [T1].[SurveyID] = [t3].[SurveyID] and t1.[SurveyID]=t2.zrefPRogramId
					  where t1.[OrgID]='61239'
					group by  t1.[SurveyYear],  t1.[OrgID], t1.[SurveyID],t2.zrefProgramId, 
					t3.OfficialSubmitterSignatureDate, t3.OfficialSubmitterSignature , x1.id, x2.id
					
  


	--FB-00013

		select *  from [hrc_fnc_migration].dbo.tbl_attributes_account t
		where [t].[AttributeValueID]='1279' or [t].[AttributeValueID]='1892'

		select [st].[OrgID], [t].[OrgOfficialName], t8.[Healthcare_System__c], t8.[Lobbyist_Region__c]   
		from [hrc_fnc_worknet].dbo.tblOrganization AS T1 
		inner join [hrc_fnc_migration].dbo.tblOrganization_final t on t1.[OrgID]=t.orgid 
		inner join [hrc_fnc_migration].dbo.tbl_attributes_account_1_final as t8 on t1.[OrgID]=t8.[OrgID]
		inner join [hrc_fnc_migration].dbo.tbl_attributes_account st on t1.[OrgID]=st.[OrgID]
		where t8.[Healthcare_System__c] is not null 	
		order by t1.orgid	
		select * from [hrc_fnc_migration].dbo.tbl_attributes_account  where [SF_Field_API]='Healthcare_System__c'
	--FB-00033

		select *  from [hrc_fnc_migration].dbo.tbl_attributes_account t
		where [t].[OrgID]='61479' and [t].[SF_Field_API]='HEI_Leader_Survey_Respondent__c'
		order by [t].[SF_Field_API]

		select [OrgID], HEI_Leader_Survey_Respondent__c 
		from [hrc_fnc_migration].dbo.tbl_attributes_account_1_final
		where [OrgID]='61479'

		select * 
		from [hrc_fnc_migration].dbo.tbl_attributes_account_1
		where [OrgID]='61479'
		order by [SF_Field_API]
			--test

				select  t.[OrgID] ,
                    --HEI_Leader_Survey_Respondent__c  
						Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
                                FROM    [hrc_fnc_migration].dbo.tbl_attributes_account_1 t1
                                WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='HEI_Leader_Survey_Respondent__c'
                                ORDER BY t1.[OrgID]
                              FOR
                                XML PATH('')
                              ), 1, 1, '') HEI_Leader_Survey_Respondent__c
					--ACAF_Organization_Type__c                      
					   ,Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
                                FROM    [hrc_fnc_migration].dbo.tbl_attributes_account_1 t1
                                WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='ACAF_Organization_Type__c'
                                ORDER BY t1.[OrgID]
                              FOR
                                XML PATH('')
                              ), 1, 1, '') ACAF_Organization_Type__c
					--ACAF_Primary_services_Provided__c
						,Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
                                FROM    [hrc_fnc_migration].dbo.tbl_attributes_account_1 t1
                                WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='ACAF_Primary_services_Provided__c'
                                ORDER BY t1.[OrgID]
                              FOR
                                XML PATH('')
                              ), 1, 1, '') ACAF_Primary_services_Provided__c
                   
                FROM    [hrc_fnc_migration].dbo.tbl_attributes_account_1  t
				WHERE   t.[SF_Field_API] ='ACAF_Organization_Type__c' or t.[SF_Field_API] ='HEI_Leader_Survey_Respondent__c'
						or t.[SF_Field_API]='HEI_Leader_Survey_Respondent__c'
                GROUP BY t.[OrgID]
			 