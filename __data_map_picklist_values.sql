USE [hrc_fnc_migration]
GO

/*generate master list of picklist values. 
  Use table __T1_ALL_FIELDS to get list of piclist field, and use 'with' code to turn columns into rows
	---src: https://stackoverflow.com/questions/18026236/sql-server-columns-to-rows
*/


--1) pull master list of objects and fields. 
		SELECT DISTINCT SF_Object, SF_Field_API, ',t.'+SF_Field_API AS 'script'
		FROM [hrc_fnc_maps].DBO.__CUSTOM_FIELDS 
		WHERE  Data_Type LIKE '%PICK%'
		GROUP BY SF_Object, SF_Field_API, Data_Type
		ORDER BY SF_Object, SF_Field_API
	 
--2) picklist values. select list of fields from qry above and copy/paste here.. 
	 --copy/paste object name to replace in IMP_XXXXXXXX and in 'XXXXXXXX' ObjectName

;with CTE1 as (
select  
(select  
 
 t.Applies_To__c
,t.Global_Response__c
,t.Response__c
,t.Support_Type__c
,t.Survey_Year__c
 



FOR xml raw('row'), type) as Data
from [hrc_fnc_migration].DBO.IMP_POLICIES_AND_MANAGEMENT as t   /*REPLACE IMP_XXXXXXX */
), CTE2 as (
SELECT
 		F.C.value('local-name(.)', 'nvarchar(128)') as FieldName,
		F.C.value('.', 'nvarchar(max)') as FieldValue
FROM CTE1 AS c
	outer apply c.Data.nodes('row/@*') as F(C)
) SELECT DISTINCT 
--'' AS ObjectName,
CTE2.FieldName	 AS 'POLICIES_AND_MANAGEMENT FieldName',	/*REPLACE  XXXXXXX */
CTE2.FieldValue  AS 'POLICIES_AND_MANAGEMENT FieldValue'	/*REPLACE  XXXXXXX */	
FROM CTE2 
WHERE CTE2.FieldName IS NOT NULL AND FieldValue IS NOT NULL 
ORDER BY FieldName, FieldValue

  