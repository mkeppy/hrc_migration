use hrc_worknet
go 

select t.*, t2.[TypeDesc] 
from [dbo].[tblOrganization] t
left join dbo.[tlkOrgType] as t2 on [t].[OrgTypeID] = [t2].[TypeID] 


select * from [dbo].[tlkOrgIndustry] t
left join dbo.[tlkOrgType] as t2 on [t].[OrgTypeID] = [t2].[TypeID] 

select distinct [StatusAbbrev] from [dbo].zOBS_tlkPersonStatus

select * from zOBS_tlkPersonType

select distinct [TypeID] from [dbo].[tblPersonType]


use [hrc_worknet]
go


--test pivot tbl
	select t.[OrgMgmtID], [t].[OrgID], [t].[SurveyYear], [t].[ManagementID], t2.[EndUserDesc] as ManagementDesc, [t].[Included], [t].[GlobalInclusion], [t].[Extent], [t].[LanguageUsed], [t].[EffectiveDate], 
	[t].[LastUpdatedByStaffID], [t].[DateEntered], [t].[MostCurrentInd] 
	from [dbo].[tblOrgManagement] as t
	left join [dbo].[tlkManagementType] as t2 on [t].[ManagementID] = [t2].[ManagementID]
	order by [t].[OrgID], [t].[SurveyYear] desc, [t].[ManagementID]





			EXEC FA_PIDI_FNL_MIGRATION.dbo.HC_PivotWizard_p		'ACCOUNT_ID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new phone types
										'SF_FIELD_VALUE',										--phone numbers
										'FA_PIDI_FNL_MIGRATION.dbo.TBL_PHONE_ACCOUNT_Seq_1_final',			--INTO..     
										'FA_PIDI_FNL_MIGRATION.dbo.TBL_PHONE_ACCOUNT_Seq_1',					--FROM..
										'SF_FIELD_VALUE is not null'							--WHERE..
