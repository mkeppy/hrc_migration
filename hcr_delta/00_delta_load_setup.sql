/*

Data Included in Delta Load
�	New Contacts from tblPerson and tblPersonType
�	New Accounts from tblOrganization
�	New Survey Responses from tblOrgSurveyResponse
�	New Activities from tblInteraction

�	New Policy/Management/Sponsorship/Benefit data from tblOrgPolicy, tblOrgManagement, tblOrgSponsorship, and tblOrgBenefit


*/

--dbp.[hrc_delta_worknet].dbo.tblOrgManagement 
		select count(*) from [hrc_fnc_worknet].dbo.tblOrgManagement 

		select * from [hrc_fnc_worknet].dbo.tblOrgManagement 
		order by [DateEntered]  desc

		select * from [hrc_delta_worknet].dbo.tblOrgManagement 
		where [DateEntered] > '2017-11-30 00:00:00.000'
		order by [DateEntered]  desc

--[hrc_delta_worknet].dbo.tblOrgBenefit
		select count(*) from [hrc_fnc_worknet].dbo.tblOrgBenefit 
		
		select * from [hrc_fnc_worknet].dbo.tblOrgBenefit 
		order by [DateEntered]  desc

		select * from [hrc_delta_worknet].dbo.tblOrgBenefit 
		where [DateEntered] > '2017-11-22 18:05:24.927'
		order by [DateEntered]  desc


--[hrc_delta_worknet].dbo.tblOrgPolicy
		select count(*) from [hrc_fnc_worknet].dbo.tblOrgPolicy 


		select * from [hrc_fnc_worknet].dbo.tblOrgPolicy 
		order by [DateEntered]  desc

		select * from [hrc_delta_worknet].dbo.tblOrgPolicy 
		where [DateEntered] > '2017-11-22 18:05:24.997'
		order by [DateEntered]  desc


--[hrc_delta_worknet].dbo.tblOrgSponsorship
		select count(*) from [hrc_fnc_worknet].dbo.tblOrgSponsorship 

		select * from [hrc_fnc_worknet].dbo.tblOrgSponsorship 
		order by [OrgSponsorshipID]
 
		select * from [hrc_delta_worknet].dbo.tblOrgSponsorship 
		order by [OrgSponsorshipID]		

--dbo.[tblInteraction] 
	select count(*) from [hrc_fnc_worknet].dbo.[tblInteraction] 
 	select count(*) from [hrc_delta_worknet].dbo.[tblInteraction] 

		select * from [hrc_fnc_worknet].dbo.[tblInteraction] 
		order by [DateCreated] desc


		select * from [hrc_delta_worknet].dbo.[tblInteraction] 
		--where datecreated > '2017-12-01 00:34:39.587'
		order by [DateCreated] desc


-- tblOrgSurveyResponse

		select * from [hrc_fnc_worknet].dbo.tblOrgSurveyResponse
		select * from [hrc_delta_worknet].dbo.tblOrgSurveyResponse 

		select d.*, f.*
		from [hrc_delta_worknet].dbo.tblOrgSurveyResponse as d
		left join [hrc_fnc_worknet].dbo.tblOrgSurveyResponse as f on d.[SurveyResponseID]=f.[SurveyResponseID]
		order by d.[SurveyResponseID]



--ORGANIZATION
--final
	select t1.[OrgID], t1.[OrgOfficialName], t1.[DateEntered], t1.[DateLastUpdated] 
	from [hrc_fnc_worknet].dbo.tblOrganization AS T1
	where t1.[DateEntered] > '2017-12-01 00:34:38.560'
	order by t1.[DateEntered] desc

--PERSON
--final
	select t1.[OrgID], t1.[FirstName], t1.[LastName], t1.[DateEntered], t1.[DateLastUpdated] 
	from [hrc_fnc_worknet].dbo.[tblPerson] AS T1
	 where t1.[DateEntered] > '2017-12-01 18:44:10.493'
	order by t1.[DateEntered] desc

--delta
	select t1.[OrgID], t1.[FirstName], t1.[LastName], t1.[DateEntered], t1.[DateLastUpdated] 
	from [hrc_delta_worknet].dbo.[tblPerson] AS T1
	where t1.[DateEntered] > '2017-12-01 18:44:10.493'
	order by t1.[DateEntered] desc


select * 
from [hrc_fnc_worknet].dbo.[tblInteraction]
where [DateCreated] > '2017-12-01 00:34:39.587'
order by [DateCreated] desc