use [hrc_delta_worknet]
go
 
 					
begin-- REFERENCE: Queries for lookup values only.
	 	
			select [AttributeValueID], [AttributeID], [AttributeValue] 
			from dbo.[tlkLookupValue] 
			--where [AttributeID]='3'   
			where [AttributeValueID]='2387' 

			select [PersonTypeID], [PersonID], [TypeID], [Notes] 
			from [hrc_delta_worknet].dbo.[tblPersonType]

			select [AttributeValueID], [AttributeID], [AttributeValue] 
			from dbo.[tlkLookupValue] 
			where [AttributeID]='3'
			
			select [AttributeValueID], [AttributeID], [AttributeValue]  
			from dbo.[tlkLookupValue]
			where  [AttributeID]='3'

			select [OrgAttributeID], [OrgID], [AttributeID], [Description] 
			from dbo.[tblAttribute] where [AttributeID]='2387'

end--lookup



	

---ATTRIBUTES for ACCOUNTS. 

			select * from [hrc_fnc_maps].dbo.[CHART_Attribute] order by [SF_Field_API]
			
			drop table  [hrc_delta_migration].dbo.tbl_attributes_account

			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], x.[AttributeValue],  t.[Description] 
			,a.SF_Object, a.SF_Field_API, 
			case when a.Translation_Rules is null then t.[description] else a.SF_Field_Value end as SF_Field_Value,  a.Translation_Rules
		  	into [hrc_delta_migration].dbo.tbl_attributes_account
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			left join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			left join [hrc_fnc_maps].dbo.chart_attribute a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
			where a.SF_Object = 'ACCOUNT'
			--test where a.Translation_Rules ='If tblAttribute.[Description]=Y, then TRUE. Else FALSE.'
			--test where t.[AttributeID]='2387'
			--test where t.description like '%Cosponsor%'
			order by t.[OrgID], t.[AttributeID]
			--tc1 205650  fnc 198901. delta full: 207608

			select distinct	sf_field_api from [hrc_delta_migration].dbo.tbl_attributes_account
			order by sf_field_api

			select * from [hrc_delta_migration].dbo.tbl_attributes_account

			--run/update based on  translation rules
				select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [SF_Field_Value] is null
				order by Translation_rules
				
				
				select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where Translation_Rules='If tblAttribute.[Description]=Y, then TRUE. Else FALSE.'
				
					--upates from translation rules/
					update  [hrc_delta_migration].dbo.tbl_attributes_account
					set [SF_Field_Value] = case when Translation_Rules='If tblAttribute.[Description]=Y, then TRUE. Else FALSE.' and [Description]='Y' then 'TRUE' else 'FALSE' end  
					where Translation_Rules='If tblAttribute.[Description]=Y, then TRUE. Else FALSE.'
					--43071   fnc 43152  delta full 43152


				select * 
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				--where t.[SF_Field_Value] is null and t.Translation_Rules is not null 
				where  ( t.Translation_Rules='Some Orgs have duplicate entries separated by a semi-colon. Only one entry is needed.')
			 
				order by t.Translation_Rules

					--upates from translation rules/
					update  [hrc_delta_migration].dbo.tbl_attributes_account
					set [SF_Field_Value] = case when Translation_Rules='Some Orgs have duplicate entries separated by a semi-colon. Only one entry is needed.' 
												and [Description] like '%;%' then SUBSTRING([description], 1, CHARINDEX(';', [description]) - 1) 
												else [Description] end  
					where Translation_Rules='Some Orgs have duplicate entries separated by a semi-colon. Only one entry is needed.'
					--6904 fnc (6904 rows affected);  delta full: 6904

			  
			  	select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [SF_Field_Value] is null
				and Translation_Rules like 'Some of the values have a%'
				and ([Description] like 'EEO:%' or [Description] like 'PND:%' or [Description] like 'Web site where posted:%')
 				order by Translation_rules

					update [hrc_delta_migration].dbo.tbl_attributes_account 
					set [SF_Field_Value] = [Description]
					where [SF_Field_Value] is null
					and Translation_Rules like 'Some of the values have a%'
					 --6817  fnc (6834 rows affected)   delta full 6979
					  
			  	select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				--Translation_Rules like 'Some of the values have a%'
				where   ([Description] like 'EEO:%' or [Description] like 'PND:%' or [Description] like 'Web site where posted:%')
				order by [SF_Field_Value]
		
					update [hrc_delta_migration].dbo.tbl_attributes_account  
					set [SF_Field_Value]=REPLACE([SF_Field_Value],'EEO:','') where [SF_Field_Value] like 'EEO:%'
					--11  fnc 11    delta full 8
					update [hrc_delta_migration].dbo.tbl_attributes_account  
					set [SF_Field_Value]=REPLACE([SF_Field_Value],'PND:','') where [SF_Field_Value] like 'PND:%'
					--34  fnc 34	  delta full 31
					update [hrc_delta_migration].dbo.tbl_attributes_account  
					set [SF_Field_Value]=REPLACE([SF_Field_Value],'Web site where posted:%','') where [SF_Field_Value] like 'Web site where posted:%'
					--234  fnc 234  delte full 110
			
				select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [SF_Field_Value] is null
				and Translation_Rules='migrate values separated by ";" as individual values'
				order by Translation_rules
				
						update [hrc_delta_migration].dbo.tbl_attributes_account 
						set [SF_Field_Value] = [Description]
						where [SF_Field_Value] is null
						and Translation_Rules='migrate values separated by ";" as individual values'
						--495  fnc 495  delta full 540

				select distinct  sf_field_value
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [t].[SF_Field_API]='PROVIDES_HIV_AIDS_SERVICES__C'
				
					   update [hrc_delta_migration].dbo.tbl_attributes_account 
					   set sf_field_value ='N'
					   where [SF_Field_API]='PROVIDES_HIV_AIDS_SERVICES__C' and [SF_Field_Value]='N/A'
					   -- fnc 1589 . delta full: 1589 
			
		
				 --- AttributeValue 2274 - add translation rule "Remove '@' symbol and Concatenate 'www.instagram.com/' + [tblAttribute].[Description]" 
				 select *, Concat('www.instagram.com/',Substring(t.[Description],2,Len([t].[Description])-1)) test
				 from [hrc_delta_migration].dbo.tbl_attributes_account t
			     where [t].[AttributeValueID]='2274' and [t].[Description] like '@%'
				 order by [t].[SF_Field_Value]
				
					 update [hrc_delta_migration].dbo.tbl_attributes_account
					 set [SF_Field_Value] = Concat('www.instagram.com/',Substring([Description],2,Len([Description])-1))
					 where [AttributeValueID]='2274' and [Description] like '@%'
						--. delta full:  188
				 -- AttributeValue 2272 - add translation rule "Remove '@' symbol and Concatenate 'www.twitter.com/' + [tblAttribute].[Description]"
				  update [hrc_delta_migration].dbo.tbl_attributes_account
				  set [Description] = LTrim([Description])
				  where [Description] is not null 
				
				 select *
				 from [hrc_delta_migration].dbo.tbl_attributes_account t
			     where [t].[AttributeValueID]='2272' and [Description] like '@%'
				 order by [t].[SF_Field_Value]
				
					 update [hrc_delta_migration].dbo.tbl_attributes_account
					 set [SF_Field_Value] =  Concat('www.twitter.com/',Substring([Description],2,Len([Description])-1))  
					 where  [AttributeValueID]='2272' and [Description] like '@%'
					 -- . delta full: 483

				 select *
				 from [hrc_delta_migration].dbo.tbl_attributes_account t
			     where ([t].[AttributeValueID]='2272'or [AttributeValueID]='2274')   and  [Description] like 'http%'
				 order by [t].[SF_Field_Value]
						 
						 update [hrc_delta_migration].dbo.tbl_attributes_account
						 set [SF_Field_Value] = [Description]
						 where ([AttributeValueID]='2272'or [AttributeValueID]='2274')   and  [Description] like 'http%'
						 --4 . delta full:  17

				 -- AttributeValue 2374 - If Attribute = "On-going LGBTQ Education Required", migrate checkbox as TRUE. Else FALSE. 
				 select *, case when [t].[Description] ='On-going LGBTQ Education Required' then 'true' else 'false' end as test 
				 from [hrc_delta_migration].dbo.tbl_attributes_account t
			     where [t].[AttributeValueID]='2374' 
				 order by [t].[SF_Field_Value]
				
					
					 update [hrc_delta_migration].dbo.tbl_attributes_account
					 set [SF_Field_Value] =  case when  [Description] ='On-going LGBTQ Education Required' then 'true' else 'false' end     
					 where  [AttributeValueID]='2374' 
					--. delta full:  8098
		 
			 	select *  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [SF_Field_Value] is null
				order by Translation_rules

				select distinct [t].[Description]  
				from [hrc_delta_migration].dbo.tbl_attributes_account t
				where [SF_Field_Value] is null
				 
					--DELETE
					delete [hrc_delta_migration].dbo.tbl_attributes_account  
					where [SF_Field_Value] is null
					--311.. fnc 632  --delta full: 7,731

				select distinct [OrgID], SF_Field_API, [SF_Field_Value]  
				into [hrc_delta_migration].dbo.tbl_attributes_account_1
				from [hrc_delta_migration].dbo.tbl_attributes_account 
				---198239  --delta full :  199847

					select *, Len([SF_Field_Value] )  
					from [hrc_delta_migration].dbo.tbl_attributes_account_1 
					where Len([SF_Field_Value] ) >255
					
					--update [hrc_delta_migration].dbo.tbl_attributes_account_1
					--set [SF_Field_Value]=Left([SF_Field_Value],255) 
					--where Len([SF_Field_Value] ) >255

 	
			    DROP TABLE  [hrc_delta_migration].dbo.tbl_attributes_account_1_final

				EXEC [hrc_delta_migration].dbo.HC_PivotWizard_p		'OrgID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new phone types
										'SF_FIELD_VALUE',										--phone numbers
										'[hrc_delta_migration].dbo.tbl_attributes_account_1_final',			--INTO..     
										'[hrc_delta_migration].dbo.tbl_attributes_account_1',					--FROM..
										'SF_FIELD_VALUE is not null and SF_Field_API NOT IN (''ACAF_Organization_Type__c'',''ACAF_Primary_services_Provided__c'',
										                                                     ''Adoption_Type__c'',  
																							 ''HEI_Leader_Survey_Respondent__c'', ''HEI_Report_Listing__c'',
																							 ''HEI_Survey_Completed_as_Indiv_Facilit__c'', ''HEI_Survey_Target__c'',
																							 ''LGBT_Household_Homestudies_Conducted__c'', ''NETWORK_Survey_Target__c'',
																							 ''Provide_Non_Placing_Family_Services__c'',  ''Public_Materials_Containing_Client_Nondi__c'')'
																							 --WHERE..  removed nulti-select picklist values. those are handled separately below

				--21512 fnc 21719  delta full: 21728

				--find duples
				select * from [hrc_delta_migration].dbo.tbl_attributes_account_1_final
				where orgid in (select orgid from [hrc_delta_migration].dbo.tbl_attributes_account_1_final group by orgid having Count(*)>1)

				--test
				select * from [hrc_delta_migration].dbo.tbl_attributes_account_1_final  where orgid='15065' order by [OrgID]
				select * from [hrc_delta_migration].dbo.tbl_attributes_account_1 where orgid='15065' order by [SF_Field_API]
	
				 select * from [hrc_delta_migration].dbo.tbl_attributes_account_1 where [SF_Field_API]='Interest_In_LGBTQ_LEG_Actions__c'
			  --multiselect picklist values. 
				
				select * 
				from [hrc_delta_migration].dbo.tbl_attributes_account_1
				where [OrgID]='61479'
				order by [SF_Field_API]
					--test
					
					select * from [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect
					DROP TABLE [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect

						select  t.[OrgID] ,
							---ACAF_Organization_Type__c
									Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='ACAF_Organization_Type__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] 
									  FOR
										XML PATH('')
									  ), 1, 1, '') ACAF_Organization_Type__c,

							--ACAF_Primary_services_Provided__c
									Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='ACAF_Primary_services_Provided__c'
										ORDER BY t1.[OrgID], [SF_Field_Value]
									  FOR
										XML PATH('')
									  ), 1, 1, '') ACAF_Primary_services_Provided__c,

							--Adoption_Type__c
									Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='Adoption_Type__c'
										ORDER BY t1.[OrgID], [SF_Field_Value]
									  FOR
										XML PATH('')
									  ), 1, 1, '') Adoption_Type__c,

							--HEI_Leader_Survey_Respondent__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='HEI_Leader_Survey_Respondent__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] desc
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') HEI_Leader_Survey_Respondent__c,
							
							--HEI_Report_Listing__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='HEI_Report_Listing__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] desc
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') HEI_Report_Listing__c,
							
							--HEI_Survey_Completed_as_Indiv_Facilit__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='HEI_Survey_Completed_as_Indiv_Facilit__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] desc
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') HEI_Survey_Completed_as_Indiv_Facilit__c,

							--HEI_Survey_Target__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='HEI_Survey_Target__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] desc
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') HEI_Survey_Target__c,

							--LGBT_Household_Homestudies_Conducted__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='LGBT_Household_Homestudies_Conducted__c'
										ORDER BY t1.[OrgID], [SF_Field_Value]
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') LGBT_Household_Homestudies_Conducted__c,

							--NETWORK_Survey_Target__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='NETWORK_Survey_Target__c'
										ORDER BY t1.[OrgID], [SF_Field_Value] desc
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') NETWORK_Survey_Target__c,
							
							--Provide_Non_Placing_Family_Services__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='Provide_Non_Placing_Family_Services__c'
										ORDER BY t1.[OrgID], [SF_Field_Value]
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') Provide_Non_Placing_Family_Services__c,

							--Public_Materials_Containing_Client_Nondi__c  
								Stuff(( SELECT  '; ' + CAST(t1.[SF_Field_Value] AS NVARCHAR(255))
										FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1 t1
										WHERE   t1.[OrgID] = t.[OrgID]  and t1.[SF_Field_API] ='Public_Materials_Containing_Client_Nondi__c'
										ORDER BY t1.[OrgID], [SF_Field_Value]
									  FOR
										XML PATH('')
						 
									  ), 1, 1, '') Public_Materials_Containing_Client_Nondi__c


						 into    [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect
						FROM    [hrc_delta_migration].dbo.tbl_attributes_account_1  t
						WHERE   t.[SF_Field_API] IN ('ACAF_Organization_Type__c','ACAF_Primary_services_Provided__c',
										             'Adoption_Type__c',  
												 	 'HEI_Leader_Survey_Respondent__c', 'HEI_Report_Listing__c',
													 'HEI_Survey_Completed_as_Indiv_Facilit__c', 'HEI_Survey_Target__c',
													 'LGBT_Household_Homestudies_Conducted__c', 'NETWORK_Survey_Target__c',
													 'Provide_Non_Placing_Family_Services__c',  'Public_Materials_Containing_Client_Nondi__c')
						
						group BY t.[OrgID]
						--2633  delta full: 2663
				--find duples
				select * from [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect
				where orgid in (select orgid from [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect group by orgid having Count(*)>1)

 	
begin-- create ACCTs attr 692. 
	 DROP TABLE [hrc_delta_migration].dbo.tbl_attribute692

	 select [OrgID],   [Description]  as [Name] , 'ATTR-'+Cast(rank() over (order by [Description]) + 1000 as Varchar(20)) as Legacy_Id__c
	 into [hrc_delta_migration].dbo.tbl_attribute692
	 from [hrc_delta_worknet].dbo.[tblAttribute] 
	 where [AttributeID]='692'  and [Description]!='N/A'
	 --fnc 4944  --delta full 4965 

	 select distinct Legacy_Id__c, [Name] 
	 into [hrc_delta_migration].dbo.tbl_attribute692_acct
	 from [hrc_delta_migration].dbo.tbl_attribute692
	 order by [Legacy_Id__c]
	 --fnc 579  delta full 599

	 	select * from [hrc_delta_migration].dbo.tbl_attribute692_acct
		where [Legacy_Id__c] in (select [Legacy_Id__c] from [hrc_delta_migration].dbo.tbl_attribute692_acct group by [Legacy_Id__c] having Count(*)>1)
		
		select * from [hrc_delta_migration].dbo.tbl_attribute692_acct
 		where [Name] in (select [Name] from [hrc_delta_migration].dbo.tbl_attribute692_acct group by [Name] having Count(*)>1)

		select * from [hrc_delta_migration].dbo.tbl_attribute692_acct



end 
 
 
begin-- CONGRESSIONAL SCORECARD base tbl
		 
		DROP TABLE [hrc_delta_migration].dbo.tbl_attributes_cd

		--create base table. 
			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], a.refAttributeValueID
			, Cast(t.[OrgID] as Varchar(10)) + '_' + a.refAttributeValueID as uniqueid
			,x.[AttributeValue],  t.[Description] 
			,a.SF_Object_2 as SF_Object, a.SF_Field_API_2 as SF_Field_API, a.SF_Field_Value_2 as SF_Field_Value,  a.Translation_Rules_2 as Translation_Rules
 			
			,'sf_object_2' zrefsrc
 		    into [hrc_delta_migration].dbo.tbl_attributes_cd
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			inner join [hrc_fnc_maps].dbo.chart_attribute a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
			where a.SF_Object_2 like '%congres%'
			--test and a.refAttributeValueID='1896'
			--test and t.[OrgID]='61788'
				union all
			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], a.refAttributeValueID
			, Cast(t.[OrgID] as Varchar(10)) + '_' + a.refAttributeValueID as uniqueid
			,x.[AttributeValue],  t.[Description] 
			,a.SF_Object, a.SF_Field_API, a.SF_Field_Value, a.Translation_Rules
			,'sf_object' zrefsrc
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			inner join [hrc_fnc_maps].dbo.chart_attribute a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
			where a.SF_Object like '%congres%'
			--and test a.refAttributeValueID='1896'
	 		--and test t.[OrgID]='61788'
			order by orgid, a.refAttributeValueID, t.[AttributeID]
			--24558  --fnc 8386  delta full 
		
		--update sf_field value	
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_cd
			 
			--IF no value and no translation rule
 			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_cd
			 where sf_field_value is null  and [Translation_Rules] is null
			order by [Translation_Rules]
			
				update [hrc_delta_migration].dbo.tbl_attributes_cd
				set SF_field_value=[Description]
				where sf_field_value is null  --and [Translation_Rules] is null
				--3334   delta full 3334
			
			--IF description contains "cosponsor" then check to TRUE.
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_cd
			 where sf_field_value is null and [Translation_Rules] like 'IF description contains%' 
			order by [Translation_Rules]
		
				update [hrc_delta_migration].dbo.tbl_attributes_cd
				set SF_field_value=case when [Description] like '%cosponsor%' then 'TRUE' else 'FALSE' end 
				where sf_field_value is null and [Translation_Rules] like 'IF description contains%' 
				--125  fhnc 0
			
			--If tblAttribute.[Description]=Y, then TRUE. Else FALSE.
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_cd
		 	where  [Translation_Rules] like 'If tblAttribute%'  
			order by [Translation_Rules]
		
				update [hrc_delta_migration].dbo.tbl_attributes_cd
				set SF_field_value=case when [Description] like '%Y%' then 'TRUE' else 'FALSE' end 
				where sf_field_value is null and [Translation_Rules] like 'If tblAttribute%'  
				--227  fnc: 0
			 

			--all others
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_cd
			where sf_field_value is null and [Translation_Rules] is null
			order by [Translation_Rules]

				update [hrc_delta_migration].dbo.tbl_attributes_cd
				set SF_field_value= [Description]
				where sf_field_value is null 		
				--2
			--pivot

				select * from [hrc_delta_migration].dbo.tbl_attributes_cd
				
			    drop table [hrc_delta_migration].dbo.tbl_attributes_cd_final

				exec  [hrc_delta_migration].dbo.HC_PivotWizard_p 'uniqueid',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new values
										'SF_FIELD_VALUE',										--phone numbers
										'[hrc_delta_migration].dbo.tbl_attributes_cd_final',			--INTO..     
										'[hrc_delta_migration].dbo.tbl_attributes_cd',					--FROM..
										'SF_FIELD_VALUE is not null'							--WHERE..

				--tc 4517   fnc 4410  delta full 4410

				--find duples
				select * from [hrc_delta_migration].dbo.tbl_attributes_cd_final
				where uniqueid in (select uniqueid from [hrc_delta_migration].dbo.tbl_attributes_cd_final group by uniqueid having Count(*)>1)

				--test
				select * from [hrc_delta_migration].dbo.tbl_attributes_cd_final  where uniqueid='61829_1818' order by uniqueid
				select * from [hrc_delta_migration].dbo.tbl_attributes_cd where uniqueid='61829_1818' order by [SF_Field_API]
	
				
				--ADD field for org id and parse the unique id to obtain the org id. 

					alter table [hrc_delta_migration].dbo.tbl_attributes_cd_final
					add OrgId Varchar(20)
				
					update [hrc_delta_migration].dbo.tbl_attributes_cd_final
					set OrgId = SUBSTRING(uniqueid, 1, CHARINDEX('_', uniqueid) - 1)
				
						select OrgId, uniqueid from [hrc_delta_migration].dbo.tbl_attributes_cd_final
						select * from [hrc_delta_migration].dbo.tbl_attributes_cd_final
		 

 
--accounts --alias
begin 
	--	select * from [hrc_delta_worknet].dbo.tblOrgNameVariation  order by [OrgID]

 		select distinct t.[OrgID],  
			STUFF(( SELECT ' '+NameVariation+Char(10)
					FROM dbo.tblOrgNameVariation  as t1
						WHERE T1.[OrgID]=T.[OrgID]  
						ORDER BY T1.[OrgID], T1.NameVariation 
					FOR
						XML PATH('')
							), 1, 1, '') AS Aliases__c
		 
	 	into [hrc_delta_migration].dbo.tbl_account_alias 
		FROM dbo.tblOrgNameVariation  T
	  	order BY t.orgid
		--tc1: 1,298   fnc 1301  delta full 1302

		select * from [hrc_delta_migration].dbo.tbl_account_alias 
end 

begin --tbl industry codes

	 	select * from [hrc_delta_worknet].dbo.[tblOrgIndustry] t
		left join [hrc_delta_worknet].dbo.[tlkOrgIndustry] t1 on t.[IndustryCode]=t1.[IndustryAbbrev]
		order by [OrgID], [t].[IndustryCode]
	 
		select distinct [IndustryDesc] 
		from   [hrc_delta_worknet].dbo.[tlkOrgIndustry]  
		order by [IndustryDesc]

		DROP TABLE [hrc_delta_migration].dbo.tbl_account_industry 

		select distinct t.[OrgID],  
			STUFF(( SELECT ' '+o.[IndustryDesc]+'; '
					FROM dbo.[tblOrgIndustry]  as t1
					left join dbo.[tlkOrgIndustry] as o on t1.[IndustryCode]=o.[IndustryAbbrev]
						WHERE T1.[OrgID]=T.[OrgID]  
						ORDER BY T1.[OrgID], T1.[IndustryCode] 
					FOR
						XML PATH('')
							), 1, 1, '') AS Industry__c,
			STUFF(( SELECT ' '+o.[IndustryDesc]
					FROM dbo.[tblOrgIndustry]  as t1
					left join dbo.[tlkOrgIndustry] as o on t1.[IndustryCode]=o.[IndustryAbbrev]
						WHERE T1.[OrgID]=T.[OrgID]  and t1.[PrimaryInd]=1
						ORDER BY T1.[OrgID], T1.[IndustryCode] 
					FOR
						XML PATH('')
							), 1, 1, '') AS Primary_Industry__c
		 
	 	into [hrc_delta_migration].dbo.tbl_account_industry 
		FROM dbo.[tblOrganization]  T
		inner join dbo.[tblOrgIndustry] t2 on t.[OrgID]=t2.[OrgID]
	  	order BY Industry__c
		--tc1: 16,575    fnc 16852   delta full 16856
		
		select * from [hrc_delta_migration].dbo.tbl_account_industry 
end 

begin --create BillingCountry field
		 select * from dbo.[tblOrganization]

		 SELECT [STATE] , count(*) c
		 from  dbo.[tblOrganization]
		 where 
		 ([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
		 OR [STATE]='DC' or [STATE]='FL'
		 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
		 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
		 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
		 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
         or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
		 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
         or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
		 group by [STATE]    --'USA'
		 ORDER BY c desc
	
		alter table dbo.[tblOrganization]
		add BillingCountry Varchar(3)

		update dbo.[tblOrganization]
		set BillingCountry = 'USA'
		where([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
		 OR [STATE]='DC' or [STATE]='FL'
		 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
		 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
		 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
		 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
         or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
		 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
         or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
		 ---fnc 25018  delta full 25028
end


begin --create BillingCountry field
		 select * from dbo.[tblPerson]

		 SELECT [STATE] , count(*) c
		 from  dbo.[tblPerson]
		 where 
		 ([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
		 OR [STATE]='DC' or [STATE]='FL'
		 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
		 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
		 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
		 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
         or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
		 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
         or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
		 group by [STATE]    --'USA'
		 ORDER BY c desc
	
		alter table dbo.[tblPerson]
		add MailingCountry Varchar(3)

		update dbo.[tblPerson]
		set MailingCountry = 'USA'
		where([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
		 OR [STATE]='DC' or [STATE]='FL'
		 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
		 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
		 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
		 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
         or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
		 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
         or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
		 ---fnc 74110  delta full 74623
end



--account record type fb00029.
		select ProgramId, count(*) 
		from  [hrc_delta_worknet].dbo.[tblOrgProgram]
		group by [ProgramID]


		--create base table of all orgs with their program id. 
		select distinct t.[OrgID],  
			STUFF(( SELECT ' '+Cast(t1.[ProgramID] as Varchar(10))+';'
					FROM [hrc_delta_worknet].dbo.[tblOrgProgram]  as t1
				 	WHERE T1.[OrgID]=T.[OrgID]   
						ORDER BY T1.[OrgID] 
					FOR
						XML PATH('')
							), 1, 1, '') AS refProgramId
		 
		into [hrc_delta_migration].dbo.tbl_prg_id
		FROM [hrc_delta_worknet].dbo.[tblOrgProgram] T
 	  	order BY t.[OrgID]
		---25220  delta full 25233

		select * from [hrc_delta_migration].dbo.tbl_prg_id
		where [OrgID] in (select [OrgID] from [hrc_delta_migration].dbo.tbl_prg_id group by [OrgID] having Count(*)>1)
		order by [OrgID]

		DROP TABLE [hrc_delta_migration].dbo.tbl_recordtype

		select	[OrgID]
				,[refProgramId]
				,case when [refProgramId]  like '%94%' or [refProgramId]  like '%95%' then null  --dnc any record with any of these
					  when [refProgramId]  like '%3;%' then 'ACAF'
					  when [refProgramId]  like '%1270;%' then 'Legislative' 
					  when [refProgramId]  like '1;%' then 'CEI'
					  when [refProgramId]  like '2;%' then 'HEI'
			 		  when [refProgramId] ='1211;' then 'Standard'
					  when [refProgramId] ='2326;' then 'Standard'
					  end as recordtype
		into [hrc_delta_migration].dbo.tbl_recordtype
		from [hrc_delta_migration].dbo.tbl_prg_id
		order by [refProgramId]
		--fnc  25220  delta full 25233
		
		--test 
		 select * from [hrc_delta_migration].dbo.tbl_recordtype
		 where [refProgramId]!='1;' and  [refProgramId]!='2;' and  [refProgramId]!='3;'
		 order by [refProgramId]

begin -- dnc accounts
		DROP TABLE [hrc_delta_migration].dbo.tblOrganization_dnc
		
		--DNC Accounts with  OrgType = 1 (Federal Government) AND  tblOrganization.[OrgOfficialName] STARTS WITH "Office Of" or "Department Of"
		select t1.[OrgID], T1.[OrgOfficialName], T1.[OrgTypeID], T2.[TypeDesc], T1.[OrgOfficialName] as OfficialNameScorecard
	 	into [hrc_delta_migration].dbo.tblOrganization_dnc
		from [hrc_delta_worknet].dbo.tblOrganization AS T1 
		inner join [hrc_delta_worknet].dbo.[tlkOrgType] as t2 on t1.[OrgTypeID]=t2.[TypeID]
		where T1.[OrgTypeID]='1' and (T1.[OrgOfficialName] like 'Office Of%' or T1.[OrgOfficialName] like 'Department Of%')
		 
		union 
		--DNC Accounts with this OrgType = 4 (State & Local Government) AND tblOrganization.[OrgOfficialName] INCLUDES "County of", "City of" , "Town of", or "Village of".
		select t1.[OrgID], T1.[OrgOfficialName], T1.[OrgTypeID], T2.[TypeDesc], T1.[OrgOfficialName] as OfficialNameScorecard
		FROM [hrc_delta_worknet].dbo.tblOrganization AS T1 
		inner join [hrc_delta_worknet].dbo.[tlkOrgType] as t2 on t1.[OrgTypeID]=t2.[TypeID]
		where T1.[OrgTypeID]='4' and (T1.[OrgOfficialName] like '%County of%' or T1.[OrgOfficialName] like '%City of%' or T1.[OrgOfficialName] like '%Town of%' or T1.[OrgOfficialName] like '%Village of%')
	
		union
		--DNC ACCOUNTS with attribute value 2382 (ORG DO NOT MIGRATE)
		select [OrgID], null [OrgOfficialName], null [OrgTypeID], null [TypeDesc], null as OfficialNameScorecard
		from [hrc_delta_migration].dbo.tbl_attributes_account 
		where [AttributeID]='2382'

		union
		--DNC ACCOUNT WITH THIS ATTRIBUTES from Programs mapped to record types.  
		select [OrgID], null [OrgOfficialName], null [OrgTypeID], null [TypeDesc], null as OfficialNameScorecard  
		from [hrc_delta_migration].dbo.tbl_recordtype
		where [recordtype] is null
	 
		--tc1: 1,256  fnc 4045  delta full 4047

		select t.* 
		into [hrc_delta_migration].dbo.tblOrganization_final 
		from dbo.[tblOrganization] as t
	 	left join [hrc_delta_migration].dbo.tblOrganization_dnc t2 on [t].[OrgID] = [t2].[OrgID]
		where t2.[OrgID] is null
		--fnc 21376   delta full: 21388
		
		select * from [hrc_delta_migration].dbo.tblOrganization_dnc
		where [OrgOfficialName]  like 'Office of%'
 		order by officialnamescorecard

		update [hrc_delta_migration].dbo.tblOrganization_dnc
		set OfficialNameScorecard = Replace(OfficialNameScorecard,'Office of Representative ','')
		where OfficialNameScorecard like 'Office of Representative%'
		---delta full:  431
		
		update [hrc_delta_migration].dbo.tblOrganization_dnc
		set OfficialNameScorecard = Replace(OfficialNameScorecard,'Office of Senator ','')
		where OfficialNameScorecard like 'Office of Senator%'
		--delta full:  101

		alter table [hrc_delta_migration].dbo.tblOrganization_dnc
		add OffNameSCFirst Varchar(20)
	
		alter table [hrc_delta_migration].dbo.tblOrganization_dnc
		add OffNameSCLast Varchar(20)

		update  [hrc_delta_migration].dbo.tblOrganization_dnc
		set OffNameSCFirst = SUBSTRING(OfficialNameScorecard, 1, CHARINDEX(' ', OfficialNameScorecard) - 1), 
			OffNameSCLast =  Substring(OfficialNameScorecard, CHARINDEX(' ', OfficialNameScorecard) + 1, 30)  
		where [OrgOfficialName] like 'Office of Senator%' or  [OrgOfficialName] like 'Office of Representative%'
		--delta full:  532 

		select * from [hrc_delta_migration].dbo.tblOrganization_dnc 
		where [OrgOfficialName] like 'Office of Senator%' or  [OrgOfficialName] like 'Office of Representative%'


		select count(*) from  dbo.[tblOrganization]  --25129						fnc 25378   elta full : 25392
	 	select Count(orgid) from  [hrc_delta_migration].dbo.tblOrganization_dnc group by orgid --1256   fnc  4002   delta 4006
		select count(*) from [hrc_delta_migration].dbo.tblOrganization_final --23873  fnc 21376    delta 21388
		 
			'Office of Representative%'
			'Office of Senator%'
end
begin-- dnc person records
		--person type 
		
		select * from [dbo].[tlkLookup]
	 

		select [AttributeValueID], [AttributeID], [AttributeValue] 
		from dbo.[tlkLookupValue] 
		where [AttributeID]='3'
		order by [AttributeID]
		 
		drop table [hrc_delta_migration].dbo.tbl_person_dnc
		drop table [hrc_delta_migration].dbo.tbl_person_final 
	
		--do not convert PersonID when PersonType is set to Convert = No
		select distinct t.[PersonID] 
		 into [hrc_delta_migration].dbo.tbl_person_dnc
		from dbo.[tblPersonType] as t
		inner join [hrc_fnc_maps].dbo.chart_persontype c on t.[TypeID]=c.attributevalueid
		where c.[convert]='no' or c.[AttributeValueID]='2383'
		 --fnc 24482  delta 24457

		select t.* 
	 	into [hrc_delta_migration].dbo.tbl_person_final 
		from dbo.[tblPerson] as t
	 	left join [hrc_delta_migration].dbo.tbl_person_dnc t2 on [t].[PersonID] = [t2].[PersonID]
		where t2.[PersonID] is null
		--fnc 50250   delta full :  50791 

		select count(*) from  dbo.[tblPerson]  --74493  fnc 74732  75248
	 	select count(*) from  [hrc_delta_migration].dbo.tbl_person_dnc  -- 3758  --after chart update: 8031.  fnc 24482  24457
		select count(*) from [hrc_delta_migration].dbo.tbl_person_final --70735  --after chart update: 66462  fnc 50250  50791

end 

begin --create multiselect picklist Person_Type__c
	
		select distinct t.[PersonID],  
			STUFF(( SELECT ' '+c.AttributeValue+';'
					FROM dbo.[tblPersonType]  as t1
					inner join [hrc_fnc_maps].dbo.chart_persontype c on t1.[TypeID]=c.attributevalueid
						WHERE T1.[PersonID]=T.[PersonID]  and c.[convert]='Yes'  
						ORDER BY T1.[PersonID] 
					FOR
						XML PATH('')
							), 1, 1, '') AS Person_Type__c
		 
	  	into [hrc_delta_migration].dbo.tbl_person_type
		FROM dbo.[tblPersonType]  T
		 inner join [hrc_fnc_maps].dbo.chart_persontype c2 on t.[TypeID]=c2.attributevalueid
		 where c2.[Convert]='Yes'
	  	order BY t.[PersonID]
		--tc1: 61,635  fnc 58027  (more types were set to dnc. )  delta full :58733 
	
		--test	
				select * from [hrc_delta_migration].dbo.tbl_person_type

				select t.*, c2.* from [dbo].[tblPersonType]  as t
				 inner join [hrc_fnc_maps].dbo.chart_persontype c2 on t.[TypeID]=c2.attributevalueid
				where [PersonID]='8204'

	 
	 			select * from [hrc_delta_migration].dbo.tbl_person_type where [PersonID]='8940'
				select * from [hrc_delta_migration].dbo.tbl_person_final  where [PersonID]='8940'

				select t.[PersonID], c.AttributeValue
				-- into [hrc_delta_migration].dbo.tbl_person_type
				from dbo.[tblPersonType] as t
				inner join [hrc_fnc_maps].dbo.chart_persontype c on t.[TypeID]=c.attributevalueid
				where c.[convert]='no'

  
end 


begin --tblinteraction: list of users to migrate

		select distinct [CreatedByStaffID] as StaffId
		into [hrc_delta_migration].dbo.tbl_users
		from [dbo].[tblInteraction]
		where [CreatedByStaffID] is not null 
		union 
		select distinct [StaffInvolvedID] as StaffId
		from [dbo].[tblInteraction]
		where [StaffInvolvedID] is not null 
		order by StaffId
		--fnc 42  delta 42
	
		--add Staff primary for OWnerId on Task
		alter table [dbo].[tblInteraction]
		add StaffOwnerId Varchar(20)

		select [StaffInvolvedID], [CreatedByStaffID], StaffOwnerId
		from dbo.[tblInteraction]

		update  dbo.[tblInteraction]
		set StaffOwnerId = case when [StaffInvolvedID] is not null then [StaffInvolvedID] else [CreatedByStaffID] end 

end 




begin-- create officer submitted records for Survey Response records from person type attributes
 
		  
		 select t.[AttributeValueID], t.[AttributeID], t.[AttributeValue], t1.[PersonID], t1.[FirstName], t1.[LastName]
		 ,Substring(t.[AttributeValue],7,4) as attr_year
		 ,Left(t.[AttributeValue],3) attr_type
		 ,case when t1.[FirstName] is null then t1.[LastName]  
			   when t1.[FirstName] is not null and t1.[LastName] is not null then t1.[FirstName] +' '+ t1.[LastName]  end as [name]
		 into [hrc_delta_migration].dbo.tbl_submitters
		 from dbo.[tlkLookupValue] t
		 inner join [hrc_delta_worknet].dbo.[tblPersonType] t2 on t.[AttributeValueID]=t2.[TypeID]
	     inner join [hrc_delta_worknet].dbo.[tblPerson] as t1 on t2.[PersonID]=t1.[PersonID]
		 where  t.[AttributeID]='3'   
				and (t.[AttributeValue] like '%cei%submitter%' or  t.[AttributeValue] like '%hei%submitter%') 
				and (t.[AttributeValue]!='CEI - 2018 Survey OFFICIAL SUBMITTER')  --already included with tbl tblCEIOrganizationContributor
		 order by t.[AttributeValue] desc
		 --9391  delta 10019
		 
		 --dedupe.
		 select distinct [attr_year], [attr_type], [name], [LastName], [FirstName]
		 into [hrc_delta_migration].dbo.tbl_submitters_dupe
		 from [hrc_delta_migration].dbo.tbl_submitters
		 order by [attr_year] desc, [attr_type], [LastName], [FirstName]
			
		 --9020  delta 9505
				select * from [hrc_delta_migration].dbo.tbl_submitters_dupe


		   select distinct t.attr_year,  t.[attr_type] + ': '+
					STUFF(( SELECT ' '+[name]+', '    --- do not add the table alias to the field name (.e.g. t.name)
							from [hrc_delta_migration].dbo.tbl_submitters_dupe as t1
							where t1.[attr_year]=t.[attr_year]
 							order BY t1.[attr_year] desc, t1.[LastName], t1.[FirstName]
							for
							xml PATH('')
							), 1, 1, '') AS official_submitter
			into [hrc_delta_migration].dbo.tbl_legacy_off_submit_hei
			from [hrc_delta_migration].dbo.tbl_submitters_dupe  T
 			where t.[attr_type]='hei'
		 --delta 7
		    select distinct t.attr_year,  t.[attr_type] + ': '+
					STUFF(( SELECT ' '+[name]+', '    --- do not add the table alias to the field name (.e.g. t.name)
							from [hrc_delta_migration].dbo.tbl_submitters_dupe as t1
							where t1.[attr_year]=t.[attr_year]
 							order BY t1.[attr_year] desc, t1.[LastName], t1.[FirstName]
							for
							xml PATH('')
							), 1, 1, '') AS official_submitter
			into [hrc_delta_migration].dbo.tbl_legacy_off_submit_cei
			from [hrc_delta_migration].dbo.tbl_submitters_dupe  T
 			where t.[attr_type]='cei'
			order by [attr_year] desc 
		   --delta 15 

			select *, Len(official_submitter) as ln 
			from [hrc_delta_migration].dbo.tbl_legacy_off_submit_hei
			order by ln desc
 	
			select *, Len(official_submitter) as ln 
			from [hrc_delta_migration].dbo.tbl_legacy_off_submit_cei
			order by ln desc
end 
 
 
begin--13_COSPONSORSHIP_AND_VOTE base tbl
		 
		DROP TABLE [hrc_delta_migration].dbo.tbl_attributes_covote

		--create base table. 
			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], a.refAttributeValueID
			, Cast(t.[OrgID] as Varchar(10)) + '_' + a.refAttributeValueID as uniqueid
			,x.[AttributeValue],  t.[Description] 
			,a.SF_Object_2 as SF_Object, a.SF_Field_API_2 as SF_Field_API, a.SF_Field_Value_2 as SF_Field_Value,  a.Translation_Rules_2 as Translation_Rules
 			
			,'sf_object_2' zrefsrc
 	    into [hrc_delta_migration].dbo.tbl_attributes_covote
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			inner join [hrc_fnc_maps].dbo.chart_attribute a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
			where a.SF_Object_2 like '%vote%'
			--test and a.refAttributeValueID='1896'
			--test and t.[OrgID]='61788'
		union all
			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], a.refAttributeValueID
			, Cast(t.[OrgID] as Varchar(10)) + '_' + a.refAttributeValueID as uniqueid
			,x.[AttributeValue],  t.[Description] 
			,a.SF_Object, a.SF_Field_API, a.SF_Field_Value, a.Translation_Rules
			,'sf_object' zrefsrc
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			inner join [hrc_fnc_maps].dbo.chart_attribute a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
			where a.SF_Object like '%vote%'
			--and test a.refAttributeValueID='1896'
	 		--and test t.[OrgID]='61788'
			order by orgid, a.refAttributeValueID, t.[AttributeID]
			--  --fnc 16051   delta 16176
		
		--update sf_field value	
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_covote
			 
			--IF no value and no translation rule
 			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_covote
			 where sf_field_value is null  and [Translation_Rules] is null
			order by [Translation_Rules]
			
				update [hrc_delta_migration].dbo.tbl_attributes_covote
				set SF_field_value=[Description]
				where sf_field_value is null and [Translation_Rules] is null
				--2731  delta 2731
			
			--IF description contains "cosponsor" then check to TRUE.
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_covote
			 where sf_field_value is null and [Translation_Rules] like 'IF description contains%' 
			order by [Translation_Rules]
		
				update [hrc_delta_migration].dbo.tbl_attributes_covote
				set SF_field_value=case when [Description] like '%cosponsor%' then 'TRUE' else 'FALSE' end 
				where sf_field_value is null and [Translation_Rules] like 'IF description contains%' 
				--125  delta  125
			
			--If tblAttribute.[Description]=Y, then TRUE. Else FALSE.
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_covote
		 	where  [Translation_Rules] like 'If tblAttribute%'  
			order by [Translation_Rules]
		
				update [hrc_delta_migration].dbo.tbl_attributes_covote
				set SF_field_value=case when [Description] like '%Y%' then 'TRUE' else 'FALSE' end 
				where sf_field_value is null and [Translation_Rules] like 'If tblAttribute%'  
				--227  fnc: 277  delta 227
			 

			--all others
			select * 
			from [hrc_delta_migration].dbo.tbl_attributes_covote
			where sf_field_value is null or [Translation_Rules] is null
			order by [Translation_Rules]

				update [hrc_delta_migration].dbo.tbl_attributes_covote
				set SF_field_value= [Description]
				where sf_field_value is null 		
				--13093  delta 13093
			--pivot

				select * from [hrc_delta_migration].dbo.tbl_attributes_covote
				
			    drop table [hrc_delta_migration].dbo.tbl_attributes_covote

				exec  [hrc_delta_migration].dbo.HC_PivotWizard_p 'uniqueid',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new values
										'SF_FIELD_VALUE',										--phone numbers
										'[hrc_delta_migration].dbo.tbl_attributes_covote_final',			--INTO..     
										'[hrc_delta_migration].dbo.tbl_attributes_covote',					--FROM..
										'SF_FIELD_VALUE is not null'							--WHERE..

				--tc 4517   fnc 3524  delta 3524

				--find duples
				select * from [hrc_delta_migration].dbo.tbl_attributes_covote_final
				where uniqueid in (select uniqueid from [hrc_delta_migration].dbo.tbl_attributes_covote_final group by uniqueid having Count(*)>1)

				--test
				select * from [hrc_delta_migration].dbo.tbl_attributes_covote_final  where uniqueid='61829_1818' order by uniqueid
				select * from [hrc_delta_migration].dbo.tbl_attributes_covote where uniqueid='61829_1818' order by [SF_Field_API]
	
				
				--ADD field for org id and parse the unique id to obtain the org id. 

					alter table [hrc_delta_migration].dbo.tbl_attributes_covote_final
					add OrgId Varchar(20)
				
					update [hrc_delta_migration].dbo.tbl_attributes_covote_final
					set OrgId = SUBSTRING(uniqueid, 1, CHARINDEX('_', uniqueid) - 1)
				
						select OrgId, uniqueid from [hrc_delta_migration].dbo.tbl_attributes_covote_final
						select * from [hrc_delta_migration].dbo.tbl_attributes_covote_final
end 




begin--X_OBJECTS base tbl
		 
		DROP TABLE [hrc_delta_migration].dbo.tbl_attributes_x_obj

 
			select distinct  t.[OrgID], t.[AttributeID], x.[AttributeValueID], a.refAttributeValueID  
			, Cast(t.[OrgID] as Varchar(10)) + '_' + a.refAttributeValueID as uniqueid
			,x.[AttributeValue],  t.[Description] 
			,a.SF_Object, a.SF_Field_API, t.[Description] as SF_Field_Value, a.Translation_Rules
			,'sf_object' zrefsrc
		 	into [hrc_delta_migration].dbo.tbl_attributes_x_obj
			from [hrc_delta_worknet].dbo.[tblAttribute] t
			inner join [hrc_delta_worknet].dbo.[tlkLookupValue] x on t.[AttributeID]=x.[AttributeValueID]
			inner join [hrc_fnc_maps].dbo.chart_attribute_leg a on x.[AttributeID]=a.[AttributeID] and x.[AttributeValueID]=a.[AttributeValueID]
		 	where a.SF_Object like 'x%'
			--and test a.refAttributeValueID='1896'
	 		--and test t.[OrgID]='61788'
			order by a.SF_Object, orgid, a.refAttributeValueID, t.[AttributeID]
			--  --fnc 16051  16051
		 
		 	select * 
			from [hrc_delta_migration].dbo.tbl_attributes_x_obj
			where [AttributeValueID]='1707' or [AttributeValueID]='1708'
			order by SF_Object,  SF_Field_API
			 
			 select * from [hrc_fnc_maps].dbo.chart_attribute_leg order by SF_Object,  SF_Field_API

			--pivot

				select * from [hrc_delta_migration].dbo.tbl_attributes_x_obj
				
			    drop table [hrc_delta_migration].dbo.tbl_attributes_x_obj_final

				exec  [hrc_delta_migration].dbo.HC_PivotWizard_p 'uniqueid, refAttributeValueID',	--fields to include as unique identifier/normally it is just a unique ID field.
										'SF_Field_API',									--column that stores all new values
										'SF_FIELD_VALUE',										--phone numbers
										'[hrc_delta_migration].dbo.tbl_attributes_x_obj_final',			--INTO..     
										'[hrc_delta_migration].dbo.tbl_attributes_x_obj',					--FROM..
										'SF_FIELD_VALUE is not null'							--WHERE..

				--tc 4517   fnc 3524  delta 3524

						--ADD field for org id and parse the unique id to obtain the org id. 

					alter table [hrc_delta_migration].dbo.tbl_attributes_x_obj_final
					add OrgId Varchar(20)
				
					update [hrc_delta_migration].dbo.tbl_attributes_x_obj_final
					set OrgId = SUBSTRING(uniqueid, 1, CHARINDEX('_', uniqueid) - 1)
				
						select OrgId, uniqueid from [hrc_delta_migration].dbo.tbl_attributes_x_obj_final
						select * from [hrc_delta_migration].dbo.tbl_attributes_x_obj_final

				--find duples
				select * from [hrc_delta_migration].dbo.tbl_attributes_x_obj_final
				where uniqueid in (select uniqueid from [hrc_delta_migration].dbo.tbl_attributes_x_obj_final group by uniqueid having Count(*)>1)

				--test
				select * from [hrc_delta_migration].dbo.tbl_attributes_x_obj_final  where uniqueid='61829_1818' order by uniqueid
				select * from [hrc_delta_migration].dbo.tbl_attributes_x_obj  where uniqueid='61829_1818' 
				
			 
end 


--- KW Contacts.

	select * 
	into [hrc_delta_migration].dbo.tbl_kw_contacts
	from [hrc_delta_migration].dbo.[xtr_contact]
	where [CREATEDBYID] ='0055A000006H7a9QAC'


	select * 
	into [hrc_delta_migration].dbo.tbl_contacts_lgbt_staffer_1275
	from [hrc_delta_migration].dbo.imp_contact
	where [Person_type__c] like '%LEG - LGBT Staffer%'

	
	select x2.id, x2.[NAME], x1.id, t1.[Legacy_Id__c], t1.[FirstName], t1.[LastName], t1.[Email], t1.[Person_type__c], 
	t2.id as kw_id, t2.[FIRSTNAME] as kw_firstname, t2.[LASTNAME] as kw_lastname, t2.[EMAIL] as kw_email, t2.[CREATEDBYID]kw_createdby
	from [hrc_delta_migration].dbo.tbl_contacts_lgbt_staffer_1275 as t1
	inner join [hrc_delta_migration].dbo.tbl_kw_contacts as t2 on t1.[Email]=t2.[EMAIL]
	left join [hrc_delta_migration].dbo.[xtr_contact] as x1 on t1.[Legacy_Id__c]=x1.[LEGACY_ID__C]
	left join [hrc_delta_migration].dbo.xtr_account as x2 on x1.[ACCOUNTID]=x2.id
	order by t1.[LastName], t1.[FirstName]