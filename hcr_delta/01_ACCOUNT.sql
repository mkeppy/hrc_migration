USE [hrc_delta_worknet]
GO

 
 /************************************************************************************************************************* 
 *************************************************************************************************************************
 NOTE::   ENSURE	TO INCLUDE AN UPDATE ON ALL ACCOUNTS UPDATED SINCE THE FINAL CONVERSION 
		  SO POTENTIAL NEW ATTRIBUTES ARE ADDED.  OR , BETTER YET, RUN AN UPDATE ON ALL ACCOUNTS SO FIELDS MAPPED FROM ATTRIBUTES ARE UPDATED ACCORDINGLY.  
		  HRC'S CONCERN IS THAT ANY POSSIBLE NEW ATTRIBUTE IS INCLUDED. 
*************************************************************************************************************************		  
*************************************************************************************************************************		  
*/
--Record types 
	/*   
			SELECT * FROM [hrc_delta_migration].dbo.XTR_RECORD_TYPE 
			order by sobjectype, aWHERE SOBJECTTYPE LIKE '%acc%'
			 
	*/
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_delta_migration].DBO.IMP_ACCOUNT

END 

select * from [hrc_delta_migration].dbo.[XTR_RECORD_TYPE] where [SOBJECTTYPE] ='account'

BEGIN -- CREATE IMP 

					SELECT   [hrc_delta_migration].[dbo].[fnc_OwnerId]()   AS OwnerId   
 							,case when x1.id is null then '0125A000001QC5EQAW' else x1.id end as RecordTypeId  --standard
							,Cast(T1.OrgID as Varchar(20)) AS  Legacy_Id__c		
							,T1.OrgOfficialName  AS  [Name]	
							,null as Incl_Status__c  --filler
							,null as Buyer_s_Guide_Category__c --filler
							,null as National_Sponsor__c	--filler
							,null as Legacy_Date_Last_Updated__c --filler
							,T1.OrgCommonName  AS  Common_Name__c		
							,case  when T1.Address2 is not null and t1.[Address1] is null then t1.[Address2]
								   when T1.Address2 is not null then T1.Address1 +Char(10)+T1.Address2 
	 							   else T1.[Address1] end AS  BillingStreet		
 							,T1.City  AS  BillingCity		
							,T1.State  AS  BillingState		
							,T1.Zip  AS  BillingPostalCode		
							,T1.County  AS  Billing_County__c		-- Yes/Link (tlkCounty.OrgID)
							,T1.BillingCountry  --from staging table.
							,T1.Phone  AS Phone 		
							,T1.Fax  AS  Fax		
							,T1.WebURL  AS  Website		
							,T2.[TypeDesc]  AS  [Type]	-- Migrate values as is from tlkOrgType.TypeDesc field via link  DNC Accounts with  OrgType = 1 (Federal Government) AND  tblOrganization.[OrgOfficialName] STARTS WITH "Office Of" or "Department Of"  DNC Accounts with this OrgType = 4 (State & Local Governme	-- Yes/Link (tlkOrgType.TypeId)
							,case when T1.PubliclyHeld ='Y' then 'TRUE' else 'FALSE' end AS  Publicly_Held__c	-- If [PubliclyHeld]=Y, then TRUE. Else, FALSE.	
							,T1.CurrentEmployeeNbr  AS  NumberOfEmployees		
							,T7.[AttributeValue]  AS  Organization_Source__c		
							,Cast(T1.DateEntered as Date) AS  CreatedDate		
							,t3.[FirstName] + ' ' + t3.[LastName]   as  Legacy_Created_By__c				-- Migrate tlkStaff.FirstName + " " + tlkStaff.LastName via link	-- Yes/Link (tlkStaff.StaffID)
							,t4.[FirstName] + ' ' + t4.[LastName]   AS  Legacy_Last_Modified_By__c	-- Migrate tlkStaff.FirstName + " " + tlkStaff.LastName via link	-- Yes/Link (tlkStaff.StaffID)
							,T1.OBS_CEIRatingNew  AS  OBS_CEIRatingNew__c		
							,t5.Aliases__c
							,t6.Industry__c				--Migrate IndustryDesc values as is from tlkOrgIndustry table.
							,t6.Primary_Industry__c		--IF PrimaryInd = 1 (TRUE) then migrate the IndustryDesc value from the tlkOrgIndustry link to this field on the [OrgID] Account
				 
							,t8.[AAMC_Teaching_Hospital_Health_System__c]
							,t8.[ACAF_Leader_Agency__c]
							,t8.[ACAF_Leader_Renewal__c]
							,t8.[ACAF_Leader_Renewal_is_Completed__c]
							,t9.[ACAF_Organization_Type__c]   --multiselect
							,t8.[ACAF_Primary_Services_Additional_Info__c]
							,t9.[ACAF_Primary_services_Provided__c]   --multiselect
							,t8.[ACAF_Primary_Services_Web_Page__c]
							,t8.[ACAF_Renewal_Challenges_Since_Leader__c]
							,t8.[ACAF_Renewal_Enhanced_LGBT_Outreach_Sin__c]
							,t8.[ACAF_Renewal_Innovations_on_Work_w_LGBT__c]
							,t8.[ACAF_Renewal_Opportunities_Since_Leader__c]
							,t8.[ACAF_Renewal_Staff_Turnover_New_Hire_Ra__c]
							,t8.[ACAF_Survey_General_Notes__c]
							,t9.[Adoption_Type__c]   --multiselect
							,t8.[AMA_Medical_School_Affiliation__c]
							,t8.[Assessment_Completed_as_Single_Multiple__c]
							,t8.[Bed_Size_Range__c]
							,t8.[Catholic_Church_Operated__c]
							,t8.[CEI_Brand_Name__c]
							,t8.[CEI_DP_Documentation__c]
							,t8.[CEI_Survey_General_Note__c]
							,t8.[Certified_LGB_Family_Placement_Number__c]
							,t8.[Certified_LGB_Family_Placement_Percent__c]
							,t8.[Certified_TG_Family_Placement_Number__c]
							,t8.[Certified_TG_Family_Placement_Percent__c]
							,t8.[Client_Non_Discrimination_Policy_URL__c]
							,t8.[CMS_Certified__c]
							,t8.[Congressional_District__c]
							,t8.[Customer_Feedback_Email__c]
							,t8.[Customer_Feedback_Phone__c]
							,t8.[Customer_Feedback_URL__c]
							,t8.[Do_NOT_Consider_for_Inclusion_in_Buyers__c]
							,t8.[Facebook_Page__c]
							,t8.[Facility_Admission_Restricted_to_Childre__c]
							,t8.[Families_Served_Number_Total_non_placin__c]
							,t8.[Family_Placements_Number_Total__c]
							,t8.[Foster_Adoptive_Parents_Served_Number_To__c]
							,t8.[HEI_2013_Training_History__c]
							,t8.[HEI_2014_LGBT_SMSA_Researched_Hospital__c]
							,t8.[HEI_2014_Training_History__c]
							,t8.[HEI_2016_Researched_Hospitals__c]
							,t8.[HEI_2016_Training_History__c]
							,t8.[HEI_2016_Training_Hours_Completed__c]
							,t8.[HEI_2017_Researched_Hospitals__c]
							,t8.[HEI_2017_Training_History__c]
							,t8.[HEI_2017_Training_Hours_Completed__c]
							,case when t8.[HEI_2018_Training_History__c] ='On-going LGBTQ Education Required' then 'true' else 'false' end as [HEI_2018_Training_History__c]
							,t8.[HEI_2018_Training_Hours_Completed__c]
							,t9.[HEI_Leader_Survey_Respondent__c]   --multiselect
							,t8.[HEI_Primary_Service__c]
							,t9.[HEI_Report_Listing__c]   --multiselect
					 		,t9.[HEI_Survey_Target__c]    --multiselect
							,t8.[HRC_National_Sponsor__c]
							,t8.[Innovation_Award_Applicant__c]
							,t8.[Innovative_Emerging_Best_Practice_Note__c]
							,t8.[Instagram_Handle__c]
							,t8.[Insurance_Carrier__c]
							,t8.[Interest_In_ENDA__c]
							,t8.[Interest_In_Equality_Act__c]
							,t8.[Interest_In_LGBTQ_LEG_Actions__c]
							,t8.[Is_Network_Parent_Organization__c]
							,t8.[Joint_Commission_Accreditation__c]
							,t8.[Leader_Renewal_Needs_to_be_Completed__c]
							,t8.[LGB_Families_Served_Number_non_Placing__c]
							,t8.[LGB_Foster_Adoptive_Parents_Served_Numbe__c]
							,t8.[LGBT_Friendly__c]
							,t9.[LGBT_Household_Homestudies_Conducted__c]    --multiselect
							,t8.[Lobbyist_Region__c]
							,t8.[Metropolitan_Statistical_Area_MSA__c]
							,t8.[Mission_statement_URL__c]
							,t9.[NETWORK_Survey_Target__c]      --multiselect
							,t8.[Non_Discrimination_EEO_Policy_URL__c]
							,t8.[Non_Placing_Agency__c]
							,t8.[Number_non_US_Based_Employees__c]
							,t8.[Original_Record_Source_ID__c]
							,t8.[Ownership_Type__c]
							,t8.[Participating_Agency__c]
							,t9.[Provide_Non_Placing_Family_Services__c]    --multiselect
							,t8.[Provides_HIV_AIDS_Services__c]
							,t9.[Public_Materials_Containing_Client_Nondi__c]   --multiselect
							,t8.[Secondary_Rank_Entity__c]
							,t8.[Significant_International_Locations__c]
							,t8.[Significant_US_Locations_Other_Than_HQ__C]
							,t8.[Steering_Committee__c]
							,t8.[Subsidiary__c]
							,t8.[Survey_Completed_as_Indiv_Facility__c]
							,t8.[Survey_Completed_as_Network_Name__c]
							,t8.[TG_Families_Served_number_Non_Placing__c]
							,t8.[TG_Foster_Adoptive_Parents_Served_Number__c]
							,t8.[TickerSymbol]
							,t8.[Twitter_Handle__c]
							,t8.[Union_Employee__c]
							,t8.[Union_Name__c]
							,t8.[Visitation_Non_Discrimination_Web_URL__c]
							,t8.[X100_Largest_US_Cities_in_2014__c]
							,t8.[X1000_Largest_List_for_2011__c ]
							,t8.[X200_Largest_Hospitals_Research_List__c ]
							,t8.[X2014_50_Largest_Hospitals_Researched_Ho__c]
							,t8.[X2014_50_State_Researched_Hospitals__c]
					 	
							--reference
							,t10.refprogramid as zrefprogid
							,t10.recordtype as zrefrecordtypename
						   	into [hrc_delta_migration].DBO.IMP_DELTA_ACCOUNT
 							from [hrc_delta_worknet].dbo.tblOrganization AS T1 
							inner join [hrc_delta_migration].dbo.tblOrganization_final t on t1.[OrgID]=t.orgid 
 							left join [hrc_delta_worknet].dbo.[tlkOrgType] as t2 on t1.[OrgTypeID]=t2.[TypeID]
							left join [hrc_delta_worknet].dbo.[tlkStaff] as t3 on [T1].[EnteredByStaffID] = [t3].[StaffID]
							left join [hrc_delta_worknet].dbo.[tlkStaff] as t4 on [T1].[LastUpdatedByStaffID] = [t4].[StaffID]
							left join [hrc_delta_migration].dbo.tbl_account_alias  as t5 on t1.[OrgID]=t5.orgid 
							left join [hrc_delta_migration].dbo.tbl_account_industry as t6 on t1.[OrgID]=t6.orgid
							 left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
										from [hrc_delta_worknet].dbo.[tlkLookupValue] 
										where [AttributeID]='25') as t7 on t1.[OrigSourceID]=t7.[AttributeValueID]
							left join [hrc_delta_migration].dbo.tbl_attributes_account_1_final as t8 on t1.[OrgID]=t8.[OrgID]
							left join [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect as t9 on t1.[OrgID]=t9.[OrgID] 
							left join [hrc_delta_migration].dbo.tbl_recordtype  as t10 on t1.[OrgID]=t10.orgid
							left join (select * from [hrc_delta_migration].dbo.xtr_record_type where [SOBJECTTYPE]='Account') x1 on t10.recordtype=x1.name
							--test left join  [hrc_fnc_migration].dbo.tblOrganization_final  f on t.orgid=f.[OrgID]
							--test where f.[OrgID] is null 
							where t1.[DateEntered] > '2017-12-01 00:34:39.560'  ---DELTA NEW ACCOUNT. 

END --delta: 12

BEGIN -- AUDIT

	select * FROM [hrc_delta_migration].DBO.IMP_DELTA_ACCOUNT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_delta_migration].DBO.IMP_ACCOUNT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	select * FROM [hrc_delta_migration].DBO.IMP_ACCOUNT
	where name ='' or name is null
	--9
		select * from [hrc_delta_worknet].dbo.[tblOrganization] 
		where orgid in('66075',
					'63317',
					'63457',
					'64985',
					'66075',
					'67120',
					'67259',
					'67269')
		update [hrc_delta_migration].DBO.IMP_ACCOUNT
		set [name]='Unknown'
		where [name]='' or [name] is null
	
	 
  	SELECT COUNT(*) FROM [hrc_delta_migration].DBO.IMP_ACCOUNT
	 
	SELECT * FROM [hrc_delta_migration].DBO.IMP_ACCOUNT
	 

END 


begin -- -execptions

		select * from [hrc_delta_migration].dbo.xtr_account where legacy_id__c =''
		delete [hrc_delta_migration].dbo.xtr_account where legacy_id__c =''

		select t.* from [hrc_delta_migration].dbo.imp_account t
		left join [hrc_delta_migration].dbo.xtr_account x on t.[Legacy_Id__c]=x.legacy_id__c
		where x.legacy_id__c is null or x.legacy_id__c =''

		select * from [hrc_delta_migration].dbo.xtr_account 


begin --ACCOUNT UPDATE PARENT : NO DELTA PARENT
							select Cast(t1.[OrgID] as Varchar(20)) as  zLegacy_Id__c
							,T1.ParentOrgID  AS  zParentId_Legacy_Id__c		-- Yes/link (tblOrganization.OrgID)
 						--	,x.id
						--	,case when (x3.[NAME]<>x2.[NAME]) or x2.id is null then x3.id 
						--		   else x2.id
						--		  end as Parentid

							,'tblOrganization' as zrefSrc
							,t3.legacy_id__c zrefAcctAttrLegUd
						--	 x2.[NAME] zrefParOrgName, 
						--	 t3.[NAME] zrefAcctAttrName
		    			--  INTO [hrc_delta_migration].DBO.IMP_ACCOUNT_parent
							FROM [hrc_delta_worknet].dbo.tblOrganization AS T1 
							inner join [hrc_delta_migration].dbo.tblOrganization_final t on  t1.[OrgID]=t.orgid 
							inner join [hrc_delta_migration].dbo.[IMP_DELTA_ACCOUNT] A on t.orgId=A.[Legacy_Id__c]
					--		inner join [hrc_delta_migration].dbo.xtr_account as x on Cast(t.[OrgID] as Varchar(20))=x.legacy_id__c
							left join [hrc_delta_migration].dbo.tblOrganization_final t2 on t1.[ParentOrgID]=t2.orgid 
						--	left join [hrc_delta_migration].dbo.xtr_account as x2 on Cast(t2.[OrgID] as Varchar(20))=x2.legacy_id__c
							left join [hrc_delta_migration].dbo.tbl_attribute692 as t3 on t1.[OrgID] =t3.OrgId
						--	left join [hrc_delta_migration].dbo.xtr_account as x3 on Cast(t3.legacy_id__c as Varchar(20))=x3.legacy_id__c
							 where x2.id is not null or x3.id is not null 
						 
						 
							--audit
							select * from [hrc_delta_migration].DBO.IMP_ACCOUNT_parent
							where Cast([zLegacy_Id__c] as Varchar(20))=Cast([zParentId_Legacy_Id__c] as Varchar(20))

							delete  [hrc_delta_migration].DBO.IMP_ACCOUNT_parent
							where Cast([zLegacy_Id__c] as Varchar(20))=Cast([zParentId_Legacy_Id__c] as Varchar(20))

							
							select [zLegacy_Id__c] 
							from [hrc_delta_migration].DBO.IMP_ACCOUNT_parent
							group by [zLegacy_Id__c]
							having Count(*)>1

								select * from  [hrc_delta_migration].DBO.IMP_ACCOUNT_parent
								where zlegacy_id__c ='59626'

end 