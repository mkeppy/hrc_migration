


							select x.id
							,T7.[AttributeValue]  AS  Organization_Source__c
							,t8.[AAMC_Teaching_Hospital_Health_System__c]
							,t8.[ACAF_Leader_Agency__c]
							,t8.[ACAF_Leader_Renewal__c]
							,t8.[ACAF_Leader_Renewal_is_Completed__c]
							,t9.[ACAF_Organization_Type__c]   --multiselect
							,t8.[ACAF_Primary_Services_Additional_Info__c]
							,t9.[ACAF_Primary_services_Provided__c]   --multiselect
							,t8.[ACAF_Primary_Services_Web_Page__c]
							,t8.[ACAF_Renewal_Challenges_Since_Leader__c]
							,t8.[ACAF_Renewal_Enhanced_LGBT_Outreach_Sin__c]
							,t8.[ACAF_Renewal_Innovations_on_Work_w_LGBT__c]
							,t8.[ACAF_Renewal_Opportunities_Since_Leader__c]
							,t8.[ACAF_Renewal_Staff_Turnover_New_Hire_Ra__c]
							,t8.[ACAF_Survey_General_Notes__c]
							,t9.[Adoption_Type__c]   --multiselect
							,t8.[AMA_Medical_School_Affiliation__c]
							,t8.[Assessment_Completed_as_Single_Multiple__c]
							,t8.[Bed_Size_Range__c]
							,t8.[Catholic_Church_Operated__c]
							,t8.[CEI_Brand_Name__c]
							,t8.[CEI_DP_Documentation__c]
							,t8.[CEI_Survey_General_Note__c]
							,t8.[Certified_LGB_Family_Placement_Number__c]
							,t8.[Certified_LGB_Family_Placement_Percent__c]
							,t8.[Certified_TG_Family_Placement_Number__c]
							,t8.[Certified_TG_Family_Placement_Percent__c]
							,t8.[Client_Non_Discrimination_Policy_URL__c]
							,t8.[CMS_Certified__c]
							,t8.[Congressional_District__c]
							,t8.[Customer_Feedback_Email__c]
							,t8.[Customer_Feedback_Phone__c]
							,t8.[Customer_Feedback_URL__c]
							,t8.[Do_NOT_Consider_for_Inclusion_in_Buyers__c]
							,t8.[Facebook_Page__c]
							,t8.[Facility_Admission_Restricted_to_Childre__c]
							,t8.[Families_Served_Number_Total_non_placin__c]
							,t8.[Family_Placements_Number_Total__c]
							,t8.[Foster_Adoptive_Parents_Served_Number_To__c]
							,t8.[HEI_2013_Training_History__c]
							,t8.[HEI_2014_LGBT_SMSA_Researched_Hospital__c]
							,t8.[HEI_2014_Training_History__c]
							,t8.[HEI_2016_Researched_Hospitals__c]
							,t8.[HEI_2016_Training_History__c]
							,t8.[HEI_2016_Training_Hours_Completed__c]
							,t8.[HEI_2017_Researched_Hospitals__c]
							,t8.[HEI_2017_Training_History__c]
							,t8.[HEI_2017_Training_Hours_Completed__c]
							,case when t8.[HEI_2018_Training_History__c] ='On-going LGBTQ Education Required' then 'true' else 'false' end as [HEI_2018_Training_History__c]
							,t8.[HEI_2018_Training_Hours_Completed__c]
							,t9.[HEI_Leader_Survey_Respondent__c]   --multiselect
							,t8.[HEI_Primary_Service__c]
							,t9.[HEI_Report_Listing__c]   --multiselect
					 		,t9.[HEI_Survey_Target__c]    --multiselect
							,t8.[HRC_National_Sponsor__c]
							,t8.[Innovation_Award_Applicant__c]
							,t8.[Innovative_Emerging_Best_Practice_Note__c]
							,t8.[Instagram_Handle__c]
							,t8.[Insurance_Carrier__c]
							,t8.[Interest_In_ENDA__c]
							,t8.[Interest_In_Equality_Act__c]
							,t8.[Interest_In_LGBTQ_LEG_Actions__c]
							,t8.[Is_Network_Parent_Organization__c]
							,t8.[Joint_Commission_Accreditation__c]
							,t8.[Leader_Renewal_Needs_to_be_Completed__c]
							,t8.[LGB_Families_Served_Number_non_Placing__c]
							,t8.[LGB_Foster_Adoptive_Parents_Served_Numbe__c]
							,t8.[LGBT_Friendly__c]
							,t9.[LGBT_Household_Homestudies_Conducted__c]    --multiselect
							,t8.[Lobbyist_Region__c]
							,t8.[Metropolitan_Statistical_Area_MSA__c]
							,t8.[Mission_statement_URL__c]
							,t9.[NETWORK_Survey_Target__c]      --multiselect
							,t8.[Non_Discrimination_EEO_Policy_URL__c]
							,t8.[Non_Placing_Agency__c]
							,t8.[Number_non_US_Based_Employees__c]
							,t8.[Original_Record_Source_ID__c]
							,t8.[Ownership_Type__c]
							,t8.[Participating_Agency__c]
							,t9.[Provide_Non_Placing_Family_Services__c]    --multiselect
							,t8.[Provides_HIV_AIDS_Services__c]
							,t9.[Public_Materials_Containing_Client_Nondi__c]   --multiselect
							,t8.[Secondary_Rank_Entity__c]
							,t8.[Significant_International_Locations__c]
							,t8.[Significant_US_Locations_Other_Than_HQ__C]
							,t8.[Steering_Committee__c]
							,t8.[Subsidiary__c]
							,t8.[Survey_Completed_as_Indiv_Facility__c]
							,t8.[Survey_Completed_as_Network_Name__c]
							,t8.[TG_Families_Served_number_Non_Placing__c]
							,t8.[TG_Foster_Adoptive_Parents_Served_Number__c]
							,t8.[TickerSymbol]
							,t8.[Twitter_Handle__c]
							,t8.[Union_Employee__c]
							,t8.[Union_Name__c]
							,t8.[Visitation_Non_Discrimination_Web_URL__c]
							,t8.[X100_Largest_US_Cities_in_2014__c]
							,t8.[X1000_Largest_List_for_2011__c ]
							,t8.[X200_Largest_Hospitals_Research_List__c ]
							,t8.[X2014_50_Largest_Hospitals_Researched_Ho__c]
							,t8.[X2014_50_State_Researched_Hospitals__c]
					 	
							--reference
							,t1.[OrgID] zrefORgId
					 	-- into [hrc_delta_migration].DBO.IMP_DELTA_ACCOUNT_update_attributes
 							from [hrc_delta_worknet].dbo.tblOrganization AS T1 
							inner join [hrc_delta_migration].dbo.tblOrganization_final t on t1.[OrgID]=t.orgid 
							inner join [hrc_delta_migration].dbo.xtr_account x on Cast(t1.[OrgID] as Varchar(20))=x.legacy_id__c
 			 				 left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
										from [hrc_delta_worknet].dbo.[tlkLookupValue] 
										where [AttributeID]='25') as t7 on t1.[OrigSourceID]=t7.[AttributeValueID]
							left join [hrc_delta_migration].dbo.tbl_attributes_account_1_final as t8 on t1.[OrgID]=t8.[OrgID]
							left join [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect as t9 on t1.[OrgID]=t9.[OrgID] 
							where t1.[DateLastUpdated] > '2017-12-01 00:34:39.560'  ---DELTA NEW ACCOUNT. 
						 