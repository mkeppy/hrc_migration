USE [hrc_delta_migration]
GO
  
BEGIN -- DROP IMP

	DROP TABLE [hrc_delta_migration].DBO.IMP_CONTACT

END 

BEGIN -- CREATE IMP 

					SELECT   [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 							,case when x3.id is null then '0125A000001QC5LQAW' else x3.id end as RecordTypeId  --standard
							,T1.PersonID  AS  Legacy_Id__c		
					 		,case when x.Id is null then '0015A00001yWJThQAO' else x.id end AS  AccountId	--"Unaffiliated Contacts" Account when Contacts don't have an Account. 
							,T1.PrefixName  AS  Salutation		
							,T1.FirstName  AS  FirstName		
							,T1.MiddleName  AS  Middle_Name__c		
							,T1.LastName  AS  LastName		
							,T1.SuffixName  AS  Suffix__c		
							,case when T1.Title is null then Left(t2.Notes, 128) else T1.Title end AS  Title		  --from TblORgERGPerson: If Title from tblPerson table is blank, migrate this to Contact.Title. Truncate if necessary.
							,case  when T1.Address2 is not null and t1.[Address1] is null then t1.[Address2]
								   when T1.Address2 is not null then T1.Address1 +Char(10)+T1.Address2 
	 							   else T1.[Address1] end AS  MailingStreet	-- Concatenate into MailingStreet on new line	
							,T1.City  AS  MailingCity		
							,T1.[State]  AS  MailingState		
							,T1.Zip  AS  MailingPostalCode	
							,T1.MailingCountry	
							,T1.Email  AS  Email		
							,T1.AlternateEmail  AS  Alternate_Email__c		
							,T1.Phone  AS  Phone		
							,T1.Notes  AS  Notes__c		
							,case T1.StatusAbbrev  when 'A' then 'Active' 
												   when 'N' then 'New' 
												   when 'I' then 'Inactive' 
												   when 'T' then 'Internal'-- Migrate with full word values rather than abbrevs: A = Active, N = New, I = Inactive, T = Internal	
												   else null end as  Status__c	
							,Cast(T1.DateEntered as Date) AS  CreatedDate		
							,Cast(T1.DateLastUpdated as Date)  AS  Legacy_Last_Updated_Date__c	
							,t3.[FirstName] + ' ' + t3.[LastName] AS Legacy_Created_By__c	-- Migrate the First Name + " " + Last Name from tlkStaff via the link.	-- Yes/Link(tlkStaff.StaffID)
 							,t4.[FirstName] + ' ' + t4.[LastName] AS Legacy_Last_Updated_By__c	-- Migrate the First Name + " " + Last Name from tlkStaff via the link.	-- Yes/Link
  					 		,x1.id as Employee_Resource_Campaign__c
							
							,t5.Person_type__c
							,case when t5.[Person_Type__c] like '%GEN%DON%CONTACT%' then 'true' else 'false' end as Do_Not_Email__c
						
							,t1.[OrgID] as zrefOrgId
							,t10.refProgramId as zrefOrgProgramId
							,t10.recordtype as zrefrecordtype
							,case when x.Id is null then 'Unaffiliated Contacts Account' else null end AS  zrefUnafilAcct
			 	 	
				 	into [hrc_delta_migration].DBO.IMP_DELTA_CONTACT
			
					FROM [hrc_delta_worknet].dbo.[tblPerson] AS T1
					inner join [hrc_delta_migration].dbo.tbl_person_final as t on t1.[PersonID] = t.[PersonID]  -- only convert records per tblPErsonType
			 		left join [hrc_delta_migration].dbo.xtr_account as x on  Cast(t1.[OrgID] as Varchar(20))=x.Legacy_Id__c
					left join (select [PersonID], Max([OrgERGID]) [OrgERGID], Max([Notes]) as [Notes]
							   from [hrc_delta_worknet].[dbo].[tblOrgERGPerson]  group by [PersonID]) as t2 on t1.[PersonID]=t2.[PersonID]
			 		left join [hrc_delta_migration].dbo.xtr_employee_resource_group as x1 on t2.[OrgERGID] = x1.Legacy_Id__c
			 		left join [hrc_delta_worknet].dbo.[tlkStaff] as t3 on [T1].[EnteredByStaffID] = [t3].[StaffID]
					left join [hrc_delta_worknet].dbo.[tlkStaff] as t4 on [T1].[LastUpdatedByStaffID] = [t4].[StaffID]
					left join [hrc_delta_migration].dbo.tbl_person_type as t5 on t1.[PersonID]=t5.[PersonID]
					left join [hrc_delta_migration].dbo.tbl_recordtype  as t10 on t1.[OrgID]=t10.orgid
					left join (select * from [hrc_delta_migration].dbo.xtr_record_type where [SOBJECTTYPE]='Contact') as x3 on t10.recordtype=x3.[name]
					 
					where t1.[DateEntered] > '2017-12-01 18:44:10.493'   ---DELTA NEW CONTACTS. 

 
end -- TC1: 60317  fnc: 50250


begin -- AUDIT

	SELECT * FROM [hrc_delta_migration].DBO.IMP_DELTA_CONTACT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_delta_migration].DBO.IMP_DELTA_CONTACT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	select * from [hrc_delta_migration].[dbo].IMP_DELTA_CONTACT where [LastName] is null
	--fnc 1
		update [hrc_delta_migration].[dbo].IMP_DELTA_CONTACT 
		set LastName='Unknown' 
		where [LastName] is null
	
	select * from [hrc_delta_migration].[dbo].IMP_DELTA_CONTACT 
	where [AccountId] is null or [RecordTypeId] is null 
	 -- fnc 0
 
	select COUNT(*) FROM [hrc_delta_migration].DBO.IMP_DELTA_CONTACT  
	 
	SELECT * FROM [hrc_delta_migration].DBO.IMP_DELTA_CONTACT
	order by [AccountId]


END 

	select [Legacy_Id__c], [FirstName], [LastName], [Person_type__c] 
	from [hrc_delta_migration].DBO.IMP_DELTA_CONTACT
	where [Person_type__c] like '%HEI - LEG%'
