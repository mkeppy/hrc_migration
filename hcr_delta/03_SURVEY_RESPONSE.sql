
--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM YOUR_DATA_BASE_NAME.dbo.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE  
  	*/
 
select * 
into [hrc_delta_migration].dbo.imp_program 
from [hrc_fnc_migration].dbo.[IMP_PROGRAM]

select * 
into [hrc_delta_migration].dbo.xtr_program 
from [hrc_fnc_migration].dbo.[xtr_PROGRAM]


BEGIN -- DROP IMP

	DROP TABLE [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE

END  

				 

BEGIN -- CREATE IMP 

					SELECT   [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 						  	,Cast(T1.[OrgID] as Varchar(10)) +'-'+Cast( T1.[SurveyYear] as Varchar(10)) +'-'+Cast( T1.[SurveyID]as Varchar(10))  AS  Legacy_Id__c		
						 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
						 	,x2.id  AS  Program_Survey__c	-- Link to the Program__c record created from the tblOrgProgram table	-- Yes/Link(tblOrgProgram.ProgramID)
							,Min(Cast(T1.SurveyDateSent  as Date)) as  Date_Sent__c		
							,Max(Cast(T1.SurveyResponseDate as Date)) AS  Response_Date__c		
							,T1.SurveyYear  AS  Survey_Year__c		
							,Max(T1.Notes)  AS  Notes__c		
			  
	 				 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then t3.OfficialSubmitterSignature end as Official_Submitter_Signature__c
					 		,case when Year(t3.OfficialSubmitterSignatureDate)=T1.SurveyYear then Cast(t3.[OfficialSubmitterSignatureDate] as Date) end as Official_Submitter_Signature_Date__c
							,case when T1.SurveyYear ='2018' then x3.id end as Official_Submitter__c
							,case when t6.HEI_Leader_Survey_Respondent__c like '%'+Cast(T1.SurveyYear as Varchar(30))+'%' then 'True' else 'False' end as HEI_Leader_Survey_Respondent__c

							,t7.official_submitter  as Legacy_HEI_Official_Submitter__c
							,t8.official_submitter  as Legacy_CEI_Official_Submitter__c

							--reference
							,T1.OrgID  as zrefOrgId
							,T1.SurveyID as zrefSurveyIDProgId
							,T1.SurveyYear as zrefSurveyYear
							,t2.zrefProgramId
							,t4.[PersonID] zrefOfficialSubmitterPersonId
			 				,t6.HEI_Leader_Survey_Respondent__c as zrefSurvResLeader
			 	    into [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE
 					from [hrc_delta_worknet].dbo.tblOrgSurveyResponse as T1
				 	inner join [hrc_delta_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20)) =x1.Legacy_id__c
					left join [hrc_delta_migration].dbo.imp_program as t2 on t1.SurveyID=t2.zrefProgramId
				 	left join [hrc_delta_migration].dbo.xtr_program as x2 on t2.[Name]= x2.[Name]
	 			 	left join [hrc_delta_worknet].dbo.[tblCEIOrganization] as t3 on [T1].[OrgID] = [t3].[OrgID] and [T1].[SurveyID] = [t3].[SurveyID] and t1.[SurveyID]=t2.zrefPRogramId
					
					left join [hrc_delta_worknet].dbo.tblCEIOrganizationContributor  as t5 on t3.[OfficialSubmitterID]=t5.[ContributorID]
					left join [hrc_delta_worknet].dbo.[tblPerson] as t4 on t5.[PersonID]=t4.[PersonID]
					left join [hrc_delta_migration].dbo.xtr_contact as x3 on t4.[PersonID]=x3.[LEGACY_ID__C]
					
					left join [hrc_delta_migration].dbo.tbl_attributes_account_1_multiselect  as t6 on [T1].[OrgID] = [t6].[OrgID]

					left join [hrc_delta_migration].dbo.tbl_legacy_off_submit_hei as t7 on t1.[SurveyYear]=t7.attr_year  --HEI
					left join [hrc_delta_migration].dbo.tbl_legacy_off_submit_cei as t8 on t1.[SurveyYear]=t8.attr_year  --CEI
					-- test where t1.[OrgID]='55030'
				 	group by  t1.[SurveyYear],  t1.[OrgID], t1.[SurveyID],t2.zrefProgramId, 
					t3.OfficialSubmitterSignatureDate, t3.OfficialSubmitterSignature , x1.id, x2.id, x3.id, t4.[PersonID],  t6.HEI_Leader_Survey_Respondent__c
					,t7.official_submitter, t8.official_submitter 
			  
					 
END -- delta: 30992

BEGIN -- AUDIT
	select count(*) from [hrc_delta_worknet].dbo.[tblOrganization]

	SELECT * FROM [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE
 	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	select Notes__c , Len(Cast(Notes__c as Varchar(max))) as le
	from [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE
	order by le desc 
	
	
	select COUNT(*) FROM [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE
	 
	select d.*, x.*
	from [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE  d
	left join [hrc_delta_migration].dbo.xtr_survey_response x on d.[Legacy_Id__c]=x.legacy_id__c
	where x.id is null
	--654

	delete [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE 
	from [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE  d
	inner join [hrc_delta_migration].dbo.xtr_survey_response x on d.[Legacy_Id__c]=x.legacy_id__c
	where x.id is not null

	select * from [hrc_delta_migration].DBO.IMP_DELTA_SURVEY_RESPONSE  
	--654
END 