USE [hrc_delta_migration]
GO
 
 --tbl to consolidate both data sources , final and delta from IMP file. 
	 select * 
	 into [hrc_delta_migration].dbo.tbl_imp_survey_response_all
	 from [hrc_fnc_migration].dbo.[IMP_SURVEY_RESPONSE]
	 union all
	 select * 
	 from [hrc_delta_migration].dbo.imp_delta_survey_response


--xtr tbl from final.
	select * 
	into [hrc_delta_migration].dbo.xtr_policy_management_type
	from [hrc_fnc_migration].dbo.xtr_policy_management_type


begin -- DROP IMP
	DROP TABLE [hrc_delta_migration].DBO.TBL_POLICIES_AND_MANAGEMENT
	DROP TABLE [hrc_delta_migration].DBO.IMP_POLICIES_AND_MANAGEMENT
END 

 
BEGIN -- CREATE tbl 
					SELECT   distinct [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 							,'OM-'+Cast(T1.OrgMgmtID as Varchar(20)) AS  Legacy_ID__c	
				 			,x1.id  as  Organization__c	
							,x2.id  AS  Survey_Response_ID__c	-- Link to the SurveyResponse record created on tblOrgSurveyResponse that matches [OrgID] and [SurveyYear]
							,x3.id  AS  Question__c	-- Migrate tlkManagementType.[ManagementDesc] value via the link value via the link
							,T1.Included  AS  Response__c	
							,T1.GlobalInclusion  AS  Global_Response__c	
							,T1.Extent  AS  Sub_Response__c	
							,T1.LanguageUsed  AS  Additional_Information__c	
							,null as Applies_To__c --filler
							,Cast(T1.EffectiveDate as Date) AS  Effective_Date__c	
							,null as Notes__c --filler
							,t2.[FirstName] + ' ' + t2.[LastName]   AS  Legacy_Last_Updated_By__c	-- Migrate FirstName + " " + LastName from tlkStaff via the link.
							,Cast(T1.DateEntered as Date) AS  Date_Entered__c	
							,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as  Survey_Year__c	
					 		
							,case T1.MostCurrentInd when 1 then 'TRUE' else 'FALSE' end AS  Most_Current__c	
							,'0120m0000004WrSAAU'  AS  RecordTypeID	-- All data from this table migrates with Record Type = "Management"
			 
							--fillers tlkBenefit
								,null as Transgender__c    
								,null as Parity__c		 
								,null as Other_Parity__c	 
								,null as Transgender_Parity__c	 
							 --fillers tlkDomesticPartnerDefinition
								,null as Applies_to_Transgender__c
								,null as Opposite_Sex_Spouse__c
								,null as Same_Sex_Partner__c
								,null as Opposite_Sex_Partner__c
								,null as General_Coverage__c
								,null as Transgender_Coverage__c
							--filler tblOrgSponsorship
								,null as Sponsorship_Date__c		
								,null as Sponsorship_Beneficiary__c		
								,null as Support_Type__c	 
								,null as Sponsorship_Description__c	
							--reference
								,T1.OrgID as zrefOrgId
								,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as zrefSurveyYear
								,'tblOrgManagement' zrefSrc
								,null as zrefBenefId
								,T1.ManagementID as zrefManagementId
								,t4.legacy_id__c  as zrefSurverResponseLegacyId
			 			 
				 	  	    into [hrc_delta_migration].DBO.tbl_policies_and_management
			
							FROM [hrc_delta_worknet].dbo.tblOrgManagement AS T1
						 	inner join [hrc_delta_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_delta_worknet].dbo.[tlkStaff] as t2 on [T1].LastUpdatedByStaffID = [t2].[StaffID]
							left join [hrc_delta_worknet].dbo.tlkManagementType as t3 on t1.[ManagementID]=t3.[ManagementID]
							left join [hrc_delta_migration].dbo.tbl_imp_survey_response_all as t4 on (case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end)=t4.[zrefSurveyYear] and t1.[OrgID]=t4.zrefOrgId
					 		left join [hrc_delta_migration].dbo.xtr_survey_response as x2 on t4.Legacy_Id__c=x2.Legacy_Id__c
							left join (select * from [hrc_delta_migration].dbo.xtr_policy_management_type where legacy_id__c like 'MA%') as x3 on 'MA-'+Cast(t3.[ManagementID] as Varchar(10))=x3.legacy_id__c
							where t1.[DateEntered] > '2017-11-30 00:00:00.000'
							-- TC1: 143,389  fnc 
						union
							select   distinct [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  				,'OB-'+Cast(T1.OrgBenefitID as varchar(10)) AS  Legacy_ID__c	
					 		,x1.Id  AS  Organization__c	
							,x2.id  as Survey_Response_ID__c	-- Link to the SurveyResponse record created on tblOrgSurveyResponse that matches [OrgID] and [SurveyYear]
							,x3.id as  Question__c	-- Migrate tlkBenefit.[BenefitDesc] via the link. Migrate additional fields from tlkBenefit table for each row in this table.
							,T1.Included  AS  Response__c	
							,T1.GlobalInclusion  AS  Global_Response__c	
							,null as Sub_Response__c --filler
							,null as Additional_Information__c --filler 
							,T3.SearchDesc  AS  Applies_To__c	-- Migrate tlkDomesticPartnerDefinition. via the link. Also migrate additional fields from tlkDomesticPartnerDefinition table to all records based on AppliesToID
							,Cast(T1.EffectiveDate  as Date) as  Effective_Date__c	
							,T1.Notes  AS  Notes__c	
							,null as Legacy_Last_Updated_By__c --filler
							,Cast(T1.DateEntered as Date) AS  Date_Entered__c	
							,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as Survey_Year__c 
							,case T1.MostCurrentInd when 1 then 'TRUE' else 'FALSE' end AS  Most_Current__c	
						 	,'0120m0000004ZiiAAE'   AS  RecordTypeID	-- Migrate all data from this table as "Benefit" Record Type
							,case t2.TGInd when 1 then 'TRUE' else 'FALSE' end AS  Transgender__c
							,case t2.ParityInd when 1 then 'TRUE' else 'FALSE' end  AS  Parity__c
							,case t2.OtherParityInd when 1 then 'TRUE' else 'FALSE' end  AS  Other_Parity__c
							,case t2.TGParityInd when 1 then 'TRUE' else 'FALSE' end  AS  Transgender_Parity__c
							,case t3.TGInd  when 1 then 'TRUE' else 'FALSE' end AS  Applies_to_Transgender__c
							,case t3.OppositeSexSpouseInd  when 1 then 'TRUE' else 'FALSE' end AS  Opposite_Sex_Spouse__c
							,case t3.SameSexPartnerInd   when 1 then 'TRUE' else 'FALSE' end as  Same_Sex_Partner__c
							,case t3.OppositeSexPartnerInd  when 1 then 'TRUE' else 'FALSE' end AS  Opposite_Sex_Partner__c
							,case t3.GeneralCoverageInd  when 1 then 'TRUE' else 'FALSE' end AS  General_Coverage__c
							,case t3.TGCoverageInd  when 1 then 'TRUE' else 'FALSE' end AS  Transgender_Coverage__c
							--filler tblOrgSponsorship
								,null as Sponsorship_Date__c		
								,null as Sponsorship_Beneficiary__c		
								,null as Support_Type__c	 
								,null as Sponsorship_Description__c	
							--refe
							,t1.[OrgID] as zrefOrgId
							,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as zrefSurveyYear
							,'tblOrgBenefit' zrefSrc
							,t1.[BenefitID] as zrefBenefId
							,null as zrefManagementId
				  			,t4.legacy_id__c  as zrefSurverResponseLegacyId

							from [hrc_delta_worknet].dbo.tblOrgBenefit as t1
						 	inner join [hrc_delta_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_delta_worknet].dbo.[tlkBenefit] as t2 on t1.[BenefitID]=t2.[BenefitID]
							left join [hrc_delta_worknet].dbo.tlkDomesticPartnerDefinition as t3 on t1.[AppliesToID]=t3.[DefinitionID]
							left join [hrc_delta_migration].dbo.tbl_imp_survey_response_all as t4 on (case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end)=t4.Survey_Year__c and t1.[OrgID]=t4.zrefOrgId
					 		left join [hrc_delta_migration].dbo.xtr_survey_response as x2 on t4.Legacy_Id__c=x2.Legacy_Id__c
  							left join (select * from [hrc_delta_migration].dbo.xtr_policy_management_type where legacy_id__c like 'BE%') as x3 on 'BE-'+Cast(t2.[BenefitID] as Varchar(10))=x3.legacy_id__c
							where t1.[DateEntered] > '2017-11-22 18:05:24.927'
						union 
							select   distinct [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  				,'OP-'+Cast(T1.OrgPolicyID  as Varchar(10)) AS  Legacy_ID__c		
						 	,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,x2.id  AS  Survey_Response_ID__c	-- Link to the SurveyResponse record created on tblOrgSurveyResponse that matches [OrgID] and [SurveyYear]	
							,x3.id  AS  Question__c	-- Migrate tlkPolicyType.[PolicyType]  value via the link.	-- Yes/Link(tlkPolicyType.PolicyTypeID)
							,T1.Included  AS  Response__c		
							,T1.GlobalIncluded  AS  Global_Response__c		
							,T1.Extent  AS  Sub_Response__c		
							,T1.LanguageUsed  AS  Additional_Information__c		
							,null as Applies_To__c --filler
							,Cast(T1.EffectiveDate as Date) AS  Effective_Date__c
							,null as Notes__c --filler	
							,t3.[FirstName] + ' ' + t3.[LastName]    AS  Legacy_Last_Updated_By__c	-- Migrate FirstName + " " + LastName from tlkStaff via the link.	-- Yes/Link(tlkStaff.StaffID)
							,Cast(T1.DateEntered  as Date) as  Date_Entered__c	
							,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as Survey_Year__c		
							,case T1.MostCurrentInd when 1 then 'TRUE' else 'FALSE' end   AS  Most_Current__c		
  							,'0120m0000004WrNAAU' AS  RecordTypeId	-- All data from this table migrates with Record Type = "Policy"	 
							 --fillers tlkBenefit
								,null as Transgender__c    
								,null as Parity__c		 
								,null as Other_Parity__c	 
								,null as Transgender_Parity__c	 
							 --fillers tlkDomesticPartnerDefinition
								,null as Applies_to_Transgender__c
								,null as Opposite_Sex_Spouse__c
								,null as Same_Sex_Partner__c
								,null as Opposite_Sex_Partner__c
								,null as General_Coverage__c
								,null as Transgender_Coverage__c
							 --filler tblOrgSponsorship
								,null as Sponsorship_Date__c		
								,null as Sponsorship_Beneficiary__c		
								,null as Support_Type__c	 
								,null as Sponsorship_Description__c	
								
								--reference
								,T1.OrgID as zrefOrgId
								,case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end as zrefSurveyYear
								,'tblOrgPolicy' zrefSrc
								,null as zrefBenefId
								,null as zrefManagementId
								,t4.legacy_id__c  as zrefSurverResponseLegacyId
 							from [hrc_delta_worknet].dbo.tblOrgPolicy as t1
						 	inner join [hrc_delta_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_delta_migration].dbo.tbl_imp_survey_response_all as t4 on (case when T1.[SurveyYear] is null then Year(t1.[DateEntered]) else t1.[SurveyYear] end)=t4.Survey_Year__c and t1.[OrgID]=t4.zrefOrgId
					 		left join [hrc_delta_migration].dbo.xtr_survey_response as x2 on t4.Legacy_Id__c=x2.Legacy_Id__c
							left join [hrc_delta_worknet].dbo.[tlkPolicyType] as t2 on t1.[PolicyTypeID]=t2.[PolicyTypeID]
							left join [hrc_delta_worknet].dbo.[tlkStaff] as t3 on [T1].LastUpdatedByStaffID = [t3].[StaffID]
  							left join (select * from [hrc_delta_migration].dbo.xtr_policy_management_type where legacy_id__c like 'PO%') as x3 on 'PO-'+Cast(t2.[PolicyTypeID] as Varchar(10))=x3.legacy_id__c							
 							where t1.[DateEntered] > '2017-11-22 18:05:24.997'
						union 
  							select   distinct [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId   
 			  				,'OS-'+Cast(T1.OrgSponsorshipID  as Varchar(10)) as  Legacy_Id__c		
							,x1.id  AS  Organization__c		-- Yes/Link(tblOrganization.OrgID)
							,x2.id  Survey_Response_ID__c
							,x3.id AS  Question__c	-- Migrate tlkSponsorshipType.[SponsorshipTypeDesc] value via the link	-- Yes/Link(tlkSponsorshipType.SponsorshipTypeID)
							,T1.Included  AS  Response__c	
							--fillers
								,null as  Global_Response__c	
								,null AS  Sub_Response__c	
								,null AS  Additional_Information__c	
								,null as  Applies_To__c --filler
								,null AS  Effective_Date__c
							,T1.Notes  AS  Notes__c	
								,null  AS  Legacy_Last_Updated_By__c	 
								,null  AS  Date_Entered__c	
								,Year(T1.SponsorshipDate) as Survey_Year__c	
						 			 
								,null as  Most_Current__c	
							,'0120m0000004WrXAAU' AS  RecordTypeID	-- Migrate all data from this table as Record Type = "Sponsorship"	
 							--fillers tlkBenefit
								,null as Transgender__c    
								,null as Parity__c		 
								,null as Other_Parity__c	 
								,null as Transgender_Parity__c	 
							 --fillers tlkDomesticPartnerDefinition
								,null as Applies_to_Transgender__c
								,null as Opposite_Sex_Spouse__c
								,null as Same_Sex_Partner__c
								,null as Opposite_Sex_Partner__c
								,null as General_Coverage__c
								,null as Transgender_Coverage__c
 							,Cast(T1.SponsorshipDate  as Date) as  Sponsorship_Date__c		
							,T1.SponsorshipBeneficiary  AS  Sponsorship_Beneficiary__c		
							,T3.[AttributeValue]  AS  Support_Type__c	-- Migrate tlkLookupValue.[AttributeValue] via the link. These are all tlkLookupValue with AttributeID=31.	-- Yes/Link(tlkLookupValue.AttributeValueID)
							,T1.SponsorshipDesc  AS  Sponsorship_Description__c		
								
							--reference
								,T1.OrgID as zrefOrgId
								,Year(T1.SponsorshipDate) as zrefSurveyYear
								,'tblOrgSponsorship' zrefSrc
								,null as zrefBenefId
								,null as   zrefManagementId
								,null  as zrefSurverResponseLegacyId
							from [hrc_delta_worknet].dbo.tblOrgSponsorship as t1
							 	inner join [hrc_delta_migration].dbo.xtr_account as x1 on Cast(t1.[OrgID] as Varchar(20))=x1.legacy_id__c
							left join [hrc_delta_migration].dbo.tbl_imp_survey_response_all as t4 on Year(T1.SponsorshipDate) =t4.Survey_Year__c and t1.[OrgID]=t4.zrefOrgId
					 		left join [hrc_delta_migration].dbo.xtr_survey_response as x2 on t4.Legacy_Id__c=x2.Legacy_Id__c
							left join [hrc_delta_worknet].dbo.[tlkSponsorshipType] as t2 on t1.[SponsorshipTypeID]=t2.[SponsorshipTypeID]
						    left join (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_delta_worknet].dbo.[tlkLookupValue] 
								where [AttributeID]='31') as t3 on t1.SupportTypeID=t3.[AttributeValueID]
  							left join (select * from [hrc_delta_migration].dbo.xtr_policy_management_type where legacy_id__c like 'SP%') as x3 on 'SP-'+Cast(t2.[SponsorshipTypeID] as Varchar(10))=x3.legacy_id__c
							
							left join [hrc_fnc_worknet].dbo.[tblOrgSponsorship] as f on T1.OrgSponsorshipID=f.OrgSponsorshipID
							where f.[OrgSponsorshipID] is null 
							order by Legacy_ID__c
							
end  --delta 14564

 

begin-- CREATE ADDITIONAL SURVEY RESPONSE RECORDS from orgid and survey year only
 	 
	 --check
		 select  
			 Organization__c		 
 			,zrefOrgId
			,zrefSurveyYear 
			,zrefSrc
		from [hrc_delta_migration].dbo.[tbl_policies_and_management] t
		where Survey_Response_ID__c is null  
		order by  zrefOrgId
			,zrefSurveyYear
	
	 drop table [hrc_delta_migration].dbo.IMP_SURVEY_RESPONSE_alt
	 
	 select  distinct
			 [hrc_delta_migration].[dbo].[fnc_OwnerId]() AS OwnerId  
			 ,Cast(zrefOrgId as Varchar(10)) +'-'+Cast(zrefSurveyYear as Varchar(10))  AS  Legacy_Id__c	
			, Organization__c		 
 			,zrefOrgId
			,zrefSurveyYear as Survey_Year__c
	 into [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt	
	 from [hrc_delta_migration].dbo.[tbl_policies_and_management] t
	 where Survey_Response_ID__c is null  
	 order by  zrefOrgId
			,zrefSurveyYear
	--329

		---test 
		select  distinct t.[zrefOrgId], t.[Survey_Year__c]  , t2.zreforgid, t2.zrefsurveyyear
		from [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt t
		left join [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE t2 on t.[zrefOrgId]=t2.[zrefOrgId] and t.[Survey_Year__c]=t2.[zrefSurveyYear]
		order by t.[zrefOrgId], t.[Survey_Year__c] 

		--test
			select  
				 Organization__c		 
 				,zrefOrgId
				,zrefSurveyYear 
				,zrefSrc
			 
			from [hrc_delta_migration].dbo.[tbl_policies_and_management] t
			where survey_response_id__c is null  
			and zreforgid='55131'
			order by	zrefOrgId
						,zrefSurveyYear

	--check duplicates
	select * from [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt 
	where [Legacy_Id__c] in (select [Legacy_Id__c] from [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt  group by [Legacy_Id__c] having Count(*)>1)

	select distinct x.id, t1.organization__c, t1.survey_response_id__c , [t1].[zrefOrgId], [t1].[zrefSurveyYear], [t1].[zrefSurveyYear], t2.[Legacy_ID__c]
	from [hrc_delta_migration].dbo.[tbl_policies_and_management] t1
	inner join [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt as t2 on t1.[zrefOrgId]=[t2].[zrefOrgId] and t1.zrefSurveyYear=t2.[Survey_Year__c]
	inner join [hrc_delta_migration].dbo.xtr_survey_response_alt as x on t2.[Legacy_Id__c]=x.legacy_Id__c
	where [t1].survey_response_id__c is null 
	order by [t1].[zrefOrgId], [t1].[zrefSurveyYear] 
	 
	update [hrc_delta_migration].dbo.tbl_policies_and_management 
	set survey_response_id__c = dp.id
  	from [hrc_delta_migration].dbo.[tbl_policies_and_management] p
	inner join (select distinct  [t1].[Legacy_ID__c], t1.[zrefOrgId], t1.[zrefSurveyYear], x.id as id
				from [hrc_delta_migration].dbo.[tbl_policies_and_management] t1
				inner join [hrc_delta_migration].dbo.IMP_DELTA_SURVEY_RESPONSE_alt as t2 on t1.[zrefOrgId]=[t2].[zrefOrgId] and t1.zrefSurveyYear=t2.[Survey_Year__c]
				inner join [hrc_delta_migration].dbo.xtr_survey_response_alt as x on t2.[Legacy_Id__c]=x.legacy_Id__c
				where [t1].survey_response_id__c is null
				) as dp on p.[Legacy_ID__c]=[dp].[Legacy_ID__c]
	--6950
	 
  select * from [hrc_delta_migration].dbo.[tbl_policies_and_management]
   

BEGIN -- AUDIT
 
	SELECT * FROM [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c


	SELECT  Sponsorship_Description__c, Len(Sponsorship_Description__c) l
	FROM [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT
	order by l desc

	SELECT * FROM [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT
	where survey_response_id__c is   null
	order by [zrefOrgId]
  	  
	SELECT * FROM [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT
	where survey_response_id__c is null  or organization__c is null or question__c is null

--CREATE IMP FILE

	select distinct *
	 into [hrc_delta_migration].DBO.IMP_DELTA_POLICIES_AND_MANAGEMENT
	from [hrc_delta_migration].DBO.tbl_POLICIES_AND_MANAGEMENT
	order by [zrefOrgId]
	--14564


	select * from [hrc_delta_migration].DBO.IMP_DELTA_POLICIES_AND_MANAGEMENT 
	order by [zrefOrgId]
END 