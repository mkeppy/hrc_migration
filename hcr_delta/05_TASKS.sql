USE [hrc_delta_migration]
GO
 
 
BEGIN -- DROP IMP

	DROP TABLE [hrc_delta_migration].DBO.IMP_TASK

END 


 
BEGIN -- CREATE IMP 

					SELECT   case when x1.id is not null then x1.id else [hrc_delta_migration].[dbo].[fnc_OwnerId]()  end as OwnerId   
 							,T1.InteractionID  AS  Legacy_ID__c		
				 			,x2.id  AS  WhatId		-- Yes/Link(tblOrganization.OrgID)
				 			,x3.id  AS  WhoId		-- Yes/Link(tblPerson.PersonID)
 							,case when t1.[InteractionDate] is not null then Cast(T1.InteractionDate  as Date) 
								  when t1.[InteractionDate] is null then Cast(t1.[DateCreated] as Date) end as  ActivityDate		
							,t2.[AttributeValue]  AS  Subtype__c	-- On tlkLookupValue, AttributeID=20 are the InteractionType values	-- Yes/link(tlkLookupValue.AttributeValueID)
							,t3.[AttributeValue]  AS  [Subject]	-- On tlkLookupValue, AttributeID=21 are the InteractionMethod values	-- Yes/link(tlkLookupValue.AttributeValueID)
							,case T1.InteractionStatus when 'C' then 'Completed/Closed'
													   when 'W' then 'HRC Waiting on Input'
												       when 'R' then 'HRC Researching'
													   when 'F' then 'HRC Follow-Up'
													   when 'P' then 'Recipient in Process'
													   when 'D' then 'Recipient Declined' end as  [Status]	-- Migrate as full words: C=Completed/Closed, W=HRC Waiting on Input, R=HRC Researching, F=HRC Follow-Up, P=Recipient in Process, D=Recipient Declined	
							,T1.[Description]  AS  [Description]		
							,Cast(t1.[DateCreated] as Date) as CreatedDate
				 
							,T1.InteractionStatus  as zrefIntStat
							,t1.[OrgID] as zrefOrgID
							,t1.StaffOwnerId  as zrefStafOwId
							,x2.[name] as zrefOrgName
							,t1.[PersonID] as zrefPersonId
							,x3.legacy_Id__c as zrefContactLegID
							,x3.[name] as zrefContactName

							,t5.[OrgOfficialName] zrefDNC_OrgName
							,t5.[OrgID] zrefDNC_OrgId
							,t6.[PersonID] zrefDNC_PersonId
						
			 		--  into [hrc_delta_migration].DBO.IMP_DELTA_TASK
			
					FROM [hrc_delta_worknet].dbo.[tblInteraction] AS T1
					left join  (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_delta_worknet].dbo.[tlkLookupValue] where [AttributeID]='20' ) as t2 on t1.[InteractionTypeID]=t2.[AttributeValueID]

					left join  (select [AttributeValueID], [AttributeID], [AttributeValue] 
								from [hrc_delta_worknet].dbo.[tlkLookupValue] where [AttributeID]='21' ) as t3 on t1.[InteractionMethodID]=t3.[AttributeValueID]
					left join [hrc_delta_migration].dbo.IMP_USER as t4 on t1.StaffOwnerId = t4.zrefStaffId
					left join [hrc_delta_migration].dbo.xtr_users as x1 on t4.UserName = x1.UserName
				 	left join [hrc_delta_migration].dbo.xtr_account as x2 on Cast(t1.[OrgID] as Varchar(20))=x2.Legacy_Id__c
				    left join [hrc_delta_migration].dbo.xtr_contact as x3 on t1.[PersonID]=x3.Legacy_Id__c
					
					--dnc 
					left join [hrc_delta_migration].dbo.tblOrganization_dnc t5 on t1.[OrgID]=t5.[OrgID]
					left join [hrc_delta_migration].dbo.tbl_person_dnc  as t6 on t1.[PersonID]=t6.[PersonID]
				 	where (x2.id is not null or x3.id is not null) and t1.[DateCreated] > '2017-12-01 00:34:39.587'   --DELTA NEW INTERACTION

	 
					 
END -- TC1: 25,106.  fnc 22852

BEGIN -- AUDIT

	SELECT * FROM [hrc_delta_migration].DBO.IMP_TASK
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [hrc_delta_migration].DBO.IMP_TASK GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [hrc_delta_migration].DBO.IMP_TASK
	SELECT * FROM [hrc_delta_migration].DBO.IMP_TASK
	SELECT distinct [Subtype__c] FROM [hrc_delta_migration].DBO.IMP_TASK

	--udpate Status when null 
	update [hrc_delta_migration].dbo.IMP_TASK
	set [Status] = 'Completed/Closed'
	where [Status] is null 

	--udpate Subject when null 
	update [hrc_delta_migration].dbo.IMP_TASK
	set [Subject] =  zrefContactName
	where [Subject] is null or [Subject]=''
	
	select * from [hrc_delta_migration].dbo.IMP_TASK where [Subject] is null

	update [hrc_delta_migration].dbo.IMP_TASK
	set [Subject] =  zrefOrgName
	where [Subject] is null and whatid is not null 
	
	 select * from [hrc_delta_migration].dbo.IMP_TASK 


END 

select * from [hrc_delta_migration].dbo.xtr_account
where legacy_id__c in (select [Legacy_Id__c] from [hrc_delta_migration].dbo.xtr_contact group by [Legacy_Id__c] having Count(*)>1)

select * from [hrc_delta_migration].dbo.xtr_contact
where legacy_id__c =''

